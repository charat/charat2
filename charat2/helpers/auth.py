from flask import abort, g, render_template, request, redirect, url_for, flash
from functools import wraps


def admin_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if g.user is None or g.user.group != "admin":
            return abort(403)
        return f(*args, **kwargs)
    return decorated_function


def log_in_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if g.user is None:
            return redirect(url_for("log_in"))
        elif g.user.group == "guest":
            flash('You must confirm your email to start using Charat. If you are experiencing issues, please email help@charatrp.com.', 'danger')
            return redirect(url_for("home"))
        return f(*args, **kwargs)
    return decorated_function

