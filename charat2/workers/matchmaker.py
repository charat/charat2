#!/usr/bin/python

import time
import json
import logging

from random import shuffle
from redis import StrictRedis
from sqlalchemy import and_
from sqlalchemy.orm import joinedload
from sqlalchemy.orm.exc import NoResultFound
from uuid import uuid4

from charat2.helpers.chat import send_message
from charat2.model import sm, ChatUser, Message, SearchedChat, Character
from charat2.model.connections import redis_pool


def check_compatibility(s1, s2):

    if s1["user_id"] == s2["user_id"]:
        logging.debug("Same user, ignoring.")
        return None

    if len(s1["search_tags"]) != 0:
        match1 = s1["search_tags"] & s2["character_tags"]
        logging.debug("%s's matches: %s" % (s1["id"], match1))
        if len(match1) == 0:
            return None
    else:
        logging.debug("%s is looking for anyone." % s1["id"])
        match1 = set()

    if len(s2["search_tags"]) != 0:
        match2 = s2["search_tags"] & s1["character_tags"]
        logging.debug("%s's matches: %s" % (s2["id"], match2))
        if len(match2) == 0:
            return None
    else:
        logging.debug("%s is looking for anyone." % s2["id"])
        match2 = set()

    tags_in_common = match1 | match2
    logging.debug("Tags in common: %s" % tags_in_common)

    return { "tags_in_common": tags_in_common }


def set_character(chat_id, searcher):
    if searcher["character_id"] is None:
        logging.debug("No character ID specified for %s." % searcher["id"])
        return
    try:
        logging.debug(
            "Setting character for %s: user %s, character %s."
            % (searcher["id"], searcher["user_id"], searcher["character_id"])
        )
        character = db.query(Character).filter(and_(
            Character.id == searcher["character_id"],
            Character.user_id == searcher["user_id"],
        )).options(joinedload(Character.user)).one()
    except NoResultFound:
        logging.debug("Character not found.")
        return
    logging.debug(
        "Found character %s [%s]." % (character.name, character.alias)
    )
    db.add(ChatUser.from_character(character, chat_id=chat_id))


if __name__ == "__main__":

    # XXX get log level from stdin
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)

    db = sm()
    redis = StrictRedis(connection_pool=redis_pool)

    searcher_ids = redis.smembers("searchers")

    while True:

        # Reset the searcher list for the next iteration.
        redis.delete("searchers")
        for searcher in searcher_ids:
            logging.debug("Waking unmatched searcher %s." % searcher)
            redis.publish("searcher:%s" % searcher, "{ \"status\": \"unmatched\" }")

        time.sleep(10)

        logging.info("Starting match loop.")

        searcher_ids = redis.smembers("searchers")

        # We can't do anything with less than 2 people, so don't bother.
        if len(searcher_ids) < 2:
            logging.info("Not enough searchers, skipping.")
            continue

        searchers = []
        for searcher in searcher_ids:
            session_id = redis.get("searcher:%s:session_id" % searcher)
            # Don't match them if they've logged out since sending the request.
            try:
                user_id = int(redis.get("session:%s" % session_id))
            except ValueError:
                continue
            try:
                character_id = int(redis.get("searcher:%s:character_id" % searcher))
            except ValueError:
                character_id = None
            searchers.append({
                "id": searcher,
                "user_id": user_id,
                "character_id": character_id,
                "search_tags": redis.smembers("searcher:%s:search_tags" % searcher),
                "character_tags": redis.smembers("searcher:%s:character_tags" % searcher),
            })
        logging.debug("Searcher list: %s" % searchers)
        shuffle(searchers)

        already_matched = set()
        # Range hack so we don't check opposite pairs or against itself.
        for n in range(len(searchers)):
            s1 = searchers[n]

            for m in range(n + 1, len(searchers)):
                s2 = searchers[m]

                if s1["id"] in already_matched or s2["id"] in already_matched:
                    continue

                logging.debug("Comparing %s and %s." % (s1["id"], s2["id"]))

                match = check_compatibility(s1, s2)
                if match is None:
                    logging.debug("No match.")
                    continue

                new_url = str(uuid4()).replace("-", "")
                logging.info(
                    "Matched %s and %s, sending to %s."
                    % (s1["id"], s2["id"], new_url)
                )
                new_chat = SearchedChat(url=new_url)
                db.add(new_chat)
                db.flush()

                if len(match["tags_in_common"]) > 0:
                    send_message(db, redis, Message(
                        chat_id=new_chat.id,
                        type="search_info",
                        text=("Tags in common: %s." % (", ".join(
                            _.replace("_", " ") for _ in sorted(match["tags_in_common"])
                        ))),
                    ))

                set_character(new_chat.id, s1)
                set_character(new_chat.id, s2)

                db.commit()

                already_matched.add(s1["id"])
                already_matched.add(s2["id"])

                match_message = json.dumps({ "status": "matched", "url": new_url })
                redis.publish("searcher:%s" % s1["id"], match_message)
                redis.publish("searcher:%s" % s2["id"], match_message)
                searcher_ids.remove(s1["id"])
                searcher_ids.remove(s2["id"])

