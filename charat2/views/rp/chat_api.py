import json
import time

from flask import abort, g, jsonify, make_response, redirect, request, url_for
from sqlalchemy import and_, func
from sqlalchemy.orm import joinedload
from sqlalchemy.orm.exc import NoResultFound

from charat2.helpers.characters import validate_character_form
from charat2.helpers.chat import (
    group_chat_only,
    mark_alive,
    send_message,
    send_userlist,
    disconnect,
    disconnect_user,
    send_quit_message,
    get_userlist,
)
from charat2.model import (
    case_options,
    Ban,
    Block,
    Character,
    ChatUser,
    GroupChat,
    Invite,
    Message,
    User,
)
from charat2.model.connections import (
    get_chat_user,
    use_db_chat,
    db_connect,
    db_commit,
    db_disconnect,
)
from charat2.model.validators import color_validator


import time
from functools import update_wrapper

from redis import Redis
redis = Redis()

class RateLimit(object):
    expiration_window = 10

    def __init__(self, key_prefix, limit, per, send_x_headers):
        self.reset = (int(time.time()) // per) * per + per
        self.key = key_prefix + str(self.reset)
        self.limit = limit
        self.per = per
        self.send_x_headers = send_x_headers
        p = redis.pipeline()
        p.incr(self.key)
        p.expireat(self.key, self.reset + self.expiration_window)
        self.current = min(p.execute()[1], limit)

    remaining = property(lambda x: x.limit + x.current)
    over_limit = property(lambda x: x.current >= x.limit)

def get_view_rate_limit():
    return getattr(g, '_view_rate_limit', None)

def on_over_limit(limit):
    return 'You hit the rate limit', 400

def ratelimit(limit, per=300, send_x_headers=True,
              over_limit=on_over_limit,
              scope_func=lambda: request.remote_addr,
              key_func=lambda: request.endpoint):
    def decorator(f):
        def rate_limited(*args, **kwargs):
            key = 'rate-limit/%s/%s/' % (key_func(), scope_func())
            rlimit = RateLimit(key, limit, per, send_x_headers)
            g._view_rate_limit = rlimit
            if over_limit is not None and rlimit.over_limit:
                return over_limit(rlimit)
            return f(*args, **kwargs)
        return update_wrapper(rate_limited, f)
    return decorator

@mark_alive
def messages():

    try:
        after = int(request.form["after"])
    except (KeyError, ValueError):
        after = 0

    # Look for stored messages first, and only subscribe if there aren't any.
    messages = g.redis.zrangebyscore("chat:%s" % g.chat_id, "(%s" % after, "+inf")

    if "joining" in request.form or g.joining:
        db_connect()
        get_chat_user()
        return jsonify({
            "users": get_userlist(g.db, g.redis, g.chat),
            "chat": g.chat.to_dict(),
            "messages": [json.loads(_) for _ in messages],
        })
    elif len(messages) != 0:
        message_dict = { "messages": [json.loads(_) for _ in messages] }
        return jsonify(message_dict)

    pubsub = g.redis.pubsub()
    # Channel for general chat messages.
    pubsub.subscribe("channel:%s" % g.chat_id)
    # Channel for messages aimed specifically at you - kicks, bans etc.
    pubsub.subscribe("channel:%s:%s" % (g.chat_id, g.user_id))

    # Get rid of the database connection here so we're not hanging onto it
    # while waiting for the redis message.
    db_commit()
    db_disconnect()

    for msg in pubsub.listen():
        if msg["type"] == "message":
            # The pubsub channel sends us a JSON string, so we return that
            # instead of using jsonify.
            resp = make_response(msg["data"])
            resp.headers["Content-type"] = "application/json"
            return resp


@mark_alive
def ping():
    return "", 204


@use_db_chat
@mark_alive
@ratelimit(limit=10, per=60)
def send():

    if (
        g.chat_user.group == "silent"
        and g.chat.creator != g.user
        and g.user.group != "admin"
    ):
        abort(403)

    if "text" not in request.form:
        abort(400)

    # Change double spaces to no-break space.
    text = request.form["text"].replace("  ", u" \u00A0")
    text = text[0:6000]
    if text == "":
        abort(400)

    message_type = request.form.get("type", "ic")

    send_message(g.db, g.redis, Message(
        chat_id=g.chat.id,
        user_id=g.user.id,
        type=message_type,
        color=g.chat_user.color,
        alias=g.chat_user.alias,
        name=g.chat_user.name,
        text=text,
    ))

    # Clear typing status so the front end doesn't have to.
    if g.redis.scard("chat:%s:sockets:%s" % (g.chat.id, g.session_id)):
        typing_key = "chat:%s:typing" % g.chat.id
        if g.redis.srem(typing_key, g.user.id):
            g.redis.publish("channel:%s:typing" % g.chat.id, json.dumps({
                "typing": list(int(_) for _ in g.redis.smembers(typing_key)),
            }))

    return "", 204


@mark_alive
def set_state():
    raise NotImplementedError


@use_db_chat
@group_chat_only
@mark_alive
def set_group():

    # Validation #1: We must be allowed to set groups.
    if not g.chat_user.can("set_group"):
        abort(403)

    # Validation #2: Set group must be lower than ours.
    # Also 400 if the group is invalid.
    set_group = request.form["group"]
    try:
        if ChatUser.group_ranks[set_group] >= g.chat_user.computed_rank:
            abort(403)
    except KeyError:
        abort(400)

    # Fetch the ChatUser we're trying to change.
    if "user_id" in request.form:
        user_condition = ChatUser.user_id == request.form["user_id"]
    elif "username" in request.form:
        user_condition = func.lower(User.username) == request.form["username"].lower()
    else:
        abort(400)
    try:
        set_chat_user, set_user = g.db.query(ChatUser, User).join(
            User, ChatUser.user_id == User.id,
        ).filter(and_(
            user_condition,
            ChatUser.chat_id == g.chat.id,
        )).one()
    except NoResultFound:
        abort(404)

    # Validation #3: Set user's group must be lower than ours.
    if set_chat_user.computed_rank >= g.chat_user.computed_rank:
        abort(403)

    # If we're changing them to their current group, just send a 204 without
    # actually doing anything.
    if set_group == set_chat_user.group:
        return "", 204

    if set_group == "mod3":
        message = ("[color=#%s]%s[/color] set [color=#%s]%s[/color] to Magical Mod. They can now silence, kick and ban other users.")
    elif set_group == "mod2":
        message = ("[color=#%s]%s[/color] set [color=#%s]%s[/color] to Cute-Cute Mod. They can now silence and kick other users.")
    elif set_group == "mod1":
        message = ("[color=#%s]%s[/color] set [color=#%s]%s[/color] to Little Mod. They can now silence other users.")
    elif set_group == "user":
        if set_chat_user.group == "silent":
            message = ("[color=#%s]%s[/color] unsilenced [color=#%s]%s[/color].")
        else:
            message = ("[color=#%s]%s[/color] removed moderator status from [color=#%s]%s[/color].")
    elif set_group == "silent":
        message = ("[color=#%s]%s[/color] silenced [color=#%s]%s[/color].")

    set_chat_user.group = set_group

    send_message(g.db, g.redis, Message(
        chat_id=g.chat.id,
        user_id=set_chat_user.user_id,
        name=set_chat_user.name,
        type="user_group",
        text=message % (
            g.chat_user.color, g.user.username,
            set_chat_user.color, set_user.username,
        ),
    ))

    return "", 204


@use_db_chat
@group_chat_only
@mark_alive
def user_action():

    action = request.form["action"]
    if action not in ("kick", "ban"):
        abort(400)

    # Validation #1: We must be allowed to perform this action.
    if not g.chat_user.can(action):
        abort(403)

    # Fetch the ChatUser we're trying to act upon.
    if "user_id" in request.form:
        user_condition = ChatUser.user_id == request.form["user_id"]
    elif "username" in request.form:
        user_condition = func.lower(User.username) == request.form["username"].lower()
    else:
        abort(400)
    try:
        set_chat_user, set_user = g.db.query(ChatUser, User).join(
            User, ChatUser.user_id == User.id,
        ).filter(and_(
            user_condition,
            ChatUser.chat_id == g.chat.id,
        )).one()
    except NoResultFound:
        abort(404)

    # Validation #2: Set user's group must be lower than ours.
    if set_chat_user.computed_rank >= g.chat_user.computed_rank:
        abort(403)

    if action == "kick":
        g.redis.publish(
            "channel:%s:%s" % (g.chat.id, set_user.id),
            "{\"exit\":\"kick\"}",
        )
        # Don't allow them back in for 30 seconds.
        kick_key = "kicked:%s:%s" % (g.chat.id, set_user.id)
        g.redis.set(kick_key, 1)
        g.redis.expire(kick_key, 30)
        # Only send a kick message if they're currently online.
        if disconnect_user(g.redis, g.chat.id, set_user.id):
            send_message(g.db, g.redis, Message(
                chat_id=g.chat.id,
                user_id=set_user.id,
                type="user_action",
                name=g.chat_user.name,
                text=(
                    "[color=#%s]%s[/color] kicked "
                    "[color=#%s]%s[/color] from the chat."
                ) % (
                    g.chat_user.color, g.user.username,
                    set_chat_user.color, set_user.username,
                )
            ))
        return "", 204

    elif action == "ban":
        # In private chats, this un-invites someone instead of banning them.
        if g.chat.publicity == "private":
            deleted = g.db.query(Invite).filter(and_(
                Invite.chat_id == g.chat.id,
                Invite.user_id == set_user.id,
            )).delete()
            # Don't send a message if there wasn't an invite.
            if not deleted:
                return "", 204
            # Unsubscribe if necessary.
            set_chat_user.subscribed = False
            ban_message = (
                "[color=#%s]%s[/color] un-invited "
                "[color=#%s]%s[/color] from the chat."
            ) % (
                g.chat_user.color,
                g.user.username,
                set_chat_user.color,
                set_user.username,
            )
        else:
            # Skip if they're already banned.
            if g.db.query(func.count('*')).select_from(Ban).filter(and_(
                Ban.chat_id == g.chat.id,
                Ban.user_id == set_user.id,
            )).scalar() != 0:
                return "", 204
            g.db.add(Ban(
                user_id=set_user.id,
                chat_id=g.chat.id,
                creator_id=g.user.id,
                reason=request.form.get("reason"),
            ))
            if request.form.get("reason") is not None:
                ban_message = (
                    "[color=#%s]%s[/color] banned "
                    "[color=#%s]%s[/color] from the chat. Reason: %s"
                ) % (
                    g.chat_user.color,
                    g.user.username,
                    set_chat_user.color,
                    set_user.username,
                    request.form["reason"],
                )
            else:
                ban_message = (
                    "[color=#%s]%s[/color] banned "
                    "[color=#%s]%s[/color] from the chat."
                ) % (
                    g.chat_user.color,
                    g.user.username,
                    set_chat_user.color,
                    set_user.username,
                )
        g.redis.publish(
            "channel:%s:%s" % (g.chat.id, set_user.id),
            "{\"exit\":\"ban\"}",
        )
        disconnect_user(g.redis, g.chat.id, set_user.id)
        send_message(g.db, g.redis, Message(
            chat_id=g.chat.id,
            user_id=set_user.id,
            type="user_action",
            name=g.chat_user.name,
            text=ban_message,
        ))
        set_chat_user.subscribed = False
        return "", 204


@use_db_chat
@group_chat_only
@mark_alive
def set_flag():

    flag = request.form["flag"]
    value = request.form["value"]
    if "flag" not in request.form or "value" not in request.form:
        abort(400)

    # Validation: We must be allowed to set flags.
    if not g.chat_user.can("set_flag"):
        abort(403)

    # Boolean flags.
    if (flag in ("autosilence", "nsfw", "typing_notifications") and value in ("on", "off")):
        new_value = value == "on"
        if new_value == getattr(g.chat, flag):
            return "", 204
        setattr(g.chat, flag, new_value)
        message = ("[color=#%%s]%%s[/color] switched %s %s.") % (flag.replace("_", " "), value)

    # Publicity is an enum because we might add options for password protected
    # or invite only chats in the future.
    elif (flag == "publicity" and value in GroupChat.publicity.type.enums):
        # Only admins can set/unset pinned and admin_only.
        admin_values = ("pinned", "admin_only")
        if (value in admin_values or g.chat.publicity in admin_values) and g.chat_user.computed_group != "admin":
            abort(403)
        if value == g.chat.publicity:
            return "", 204
        g.chat.publicity = value
        if g.chat.publicity == "private":
            message = "[color=#%s]%s[/color] set the chat to private. Only users who have been invited can enter."
        elif g.chat.publicity == "unlisted":
            message = "[color=#%s]%s[/color] unlisted the chat. Anyone can visit this chat, but only if they have the URL."
        elif g.chat.publicity == "listed":
            message = "[color=#%s]%s[/color] listed the chat. It's now listed on the public chats page."
        elif g.chat.publicity == "pinned":
            message = "[color=#%s]%s[/color] pinned the chat. It's now listed at the top of the public chats page."
        elif g.chat.publicity == "admin_only":
            message = "[color=#%s]%s[/color] restricted the chat to admins."

    else:
        abort(400)

    send_message(g.db, g.redis, Message(
        chat_id=g.chat.id,
        user_id=g.user.id,
        type="chat_meta",
        text=message % (g.chat_user.color, g.user.username),
    ))

    return "", 204


@use_db_chat
@group_chat_only
@mark_alive
def set_topic():

    # Validation: We must be allowed to set the topic.
    if not g.chat_user.can("set_topic"):
        abort(403)

    topic = request.form["topic"].strip()
    # If it hasn't changed, don't bother sending a message about it.
    if topic == g.chat.topic:
        return "", 204

    g.chat.topic = topic

    if topic == "":
        send_message(g.db, g.redis, Message(
            chat_id=g.chat.id,
            user_id=g.user.id,
            name=g.chat_user.name,
            type="chat_meta",
            text="[color=#%s]%s[/color] removed the conversation topic." % (
                g.chat_user.color, g.user.username,
            ),
        ))
    else:
        send_message(g.db, g.redis, Message(
            chat_id=g.chat.id,
            user_id=g.user.id,
            name=g.chat_user.name,
            type="chat_meta",
            text="[color=#%s]%s[/color] changed the topic to \"%s\"" % (
                g.chat_user.color, g.user.username, topic,
            ),
        ))

    return "", 204


@use_db_chat
@mark_alive
def save():

    # Remember old values so we can check if they've changed later.
    old_name = g.chat_user.name
    old_alias = g.chat_user.alias
    old_color = g.chat_user.color

    new_details = validate_character_form(request.form)
    g.chat_user.name = new_details["name"]
    g.chat_user.alias = new_details["alias"]
    g.chat_user.color = new_details["color"]
    g.chat_user.quirk_prefix = new_details["quirk_prefix"]
    g.chat_user.quirk_suffix = new_details["quirk_suffix"]
    g.chat_user.case = new_details["case"]
    g.chat_user.replacements = new_details["replacements"]
    g.chat_user.regexes = new_details["regexes"]

    # Send a message if name or alias has changed.
    if g.chat_user.name != old_name or g.chat_user.alias != old_alias:
        if g.chat_user.group == "silent":
            send_userlist(g.db, g.redis, g.chat)
        else:
            alias_string = (
                " [[color=#%s]%s[/color]]" % (g.chat_user.color, g.chat_user.alias)
                if len(g.chat_user.alias) > 0 else ""
            )
            send_message(g.db, g.redis, Message(
                chat_id=g.chat.id,
                user_id=g.user.id,
                type="user_info",
                name=g.chat_user.name,
                text=("[color=#%s]%s[/color] is now [color=#%s]%s[/color]%s.") % (
                    old_color, g.user.username,
                    g.chat_user.color, g.chat_user.name, alias_string,
                ),
            ))
    # Just refresh the user list if the color has changed.
    elif g.chat_user.color != old_color:
        send_userlist(g.db, g.redis, g.chat)

    return jsonify(g.chat_user.to_dict(include_options=True))


@use_db_chat
@mark_alive
def save_from_character():

    try:
        character = g.db.query(Character).filter(and_(
            Character.id == request.form["character_id"],
            Character.user_id == g.user.id,
        )).order_by(Character.title).one()
    except NoResultFound:
        abort(404)

    old_color = g.chat_user.color

    # Send a message if name, alias or color has changed.
    changed = (
        g.chat_user.name != character.name
        or g.chat_user.alias != character.alias
        or g.chat_user.color != character.color
    )

    g.chat_user.name = character.name
    g.chat_user.alias = character.alias
    g.chat_user.color = character.color
    g.chat_user.quirk_prefix = character.quirk_prefix
    g.chat_user.quirk_suffix = character.quirk_suffix
    g.chat_user.case = character.case
    g.chat_user.replacements = character.replacements
    g.chat_user.regexes = character.regexes

    if changed:
        if g.chat_user.group == "silent":
            send_userlist(g.db, g.redis, g.chat)
        else:
            alias_string = (
                " [[color=#%s]%s[/color]]" % (g.chat_user.color, g.chat_user.alias)
                if len(g.chat_user.alias) > 0 else ""
            )
            send_message(g.db, g.redis, Message(
                chat_id=g.chat.id,
                user_id=g.user.id,
                type="user_info",
                name=g.chat_user.name,
                text=("[color=#%s]%s[/color] is now [color=#%s]%s[/color]%s.") % (
                    old_color, g.user.username,
                    g.chat_user.color, g.chat_user.name, alias_string,
                ),
            ))

    return jsonify(g.chat_user.to_dict(include_options=True))


@use_db_chat
def save_variables():

    # Boolean variables.
    for variable in [
        "confirm_disconnect",
        "desktop_notifications",
        "show_description",
        "show_connection_messages",
        "show_ic_messages",
        "show_ooc_messages",
        "show_message_info",
        "show_bbcode",
        "show_preview",
        "ooc_on",
        "typing_notifications",
        "sound_notifications",
    ]:
        if variable not in request.form:
            continue
        if request.form[variable] not in {"on", "off"}:
            abort(400)
        setattr(g.chat_user, variable, request.form[variable] == "on")

    for variable in [
        "highlighted_user_ids",
        "blocked_user_ids",
    ]:
        if variable not in request.form:
            continue
        # Convert to a set to remove duplicates.
        temp_set = set()
        for item in request.form[variable].split(","):
            try:
                temp_set.add(int(item.strip()))
            except ValueError:
                pass
        # XXX LENGTH LIMIT?
        setattr(g.chat_user, variable, list(temp_set))
 
    if request.headers.get("X-Requested-With") != "XMLHttpRequest" and "Referer" in request.headers:
        return redirect(request.headers["Referer"])

    return "", 204


def quit():
    # Only send a message if we were already online.
    if g.user_id is None or "chat_id" not in request.form:
        abort(400)
    try:
        g.chat_id = int(request.form["chat_id"])
    except ValueError:
        abort(400)
    if disconnect(g.redis, g.chat_id, g.session_id):
        db_connect()
        get_chat_user()
        send_quit_message(g.db, g.redis, g.chat_user, g.user, g.chat)
    return "", 204

