from flask import (
    abort, g, jsonify, make_response, redirect, render_template, request,
    url_for
)
from sqlalchemy import and_
from sqlalchemy.orm import joinedload, joinedload_all
from sqlalchemy.orm.exc import NoResultFound
from uuid import uuid4

from charat2.helpers import tags_to_set
from charat2.helpers.auth import log_in_required
from charat2.helpers.tags import search_tags_from_form
from charat2.model import Character, SearchTag
from charat2.model.connections import use_db, db_commit, db_disconnect


@use_db
@log_in_required
def search_get(character_id=None):

    characters = g.db.query(Character).filter(
        Character.user_id == g.user.id,
    ).order_by(Character.title, Character.id).all()

    # If we have a character ID, make sure that character exists.
    if character_id is not None:
        try:
            character = g.db.query(Character).filter(and_(
                Character.id == character_id,
                Character.user_id == g.user.id,
            )).one()
        except NoResultFound:
            abort(404)
    else:
        character_id = g.user.default_character_id

    search_tag_query = g.db.query(SearchTag).filter(
        SearchTag.user_id == g.user.id,
    ).options(joinedload(SearchTag.tag)).all()
    search_tag_lists = {
        "fandom_wanted": [],
        "character_wanted": [],
        "gender_wanted": [],
        "misc": [],
    }
    for search_tag in search_tag_query:
        if search_tag.tag.type not in search_tag_lists:
            continue
        search_tag_lists[search_tag.tag.type].append(search_tag.alias)
    search_tags = {
        tag_type: ", ".join(tag for tag in tags)
        for tag_type, tags in search_tag_lists.iteritems()
    }

    return render_template(
        "rp/search.html",
        characters=characters,
        selected_character=character_id,
        search_tags=search_tags,
    )


@use_db
@log_in_required
def search_post():

    searcher_id = str(uuid4())

    g.redis.set("searcher:%s:session_id" % searcher_id, g.session_id)

    # Save search tags.
    g.db.query(SearchTag).filter(SearchTag.user_id == g.user.id).delete()
    g.user.search_tags += search_tags_from_form(request.form)
    g.db.flush()

    g.redis.delete("searcher:%s:search_tags" % searcher_id)
    g.redis.delete("searcher:%s:character_tags" % searcher_id)

    # Cache search tags in Redis.
    search_tag_query = g.db.query(SearchTag).filter(
        SearchTag.user_id == g.user.id,
    ).options(joinedload(SearchTag.tag)).all()
    search_tags = ["%s:%s" % (
        # Remove _wanted from tag types so they match the character tags.
        _.tag.type.replace("_wanted", ""), _.tag.name
    ) for _ in search_tag_query]
    if len(search_tags) != 0:
        g.redis.sadd("searcher:%s:search_tags" % searcher_id, *search_tags)

    # Cache character ID and tags in Redis.
    try:
        character = g.db.query(Character).filter(and_(
            Character.id == int(request.form["character_id"]),
            Character.user_id == g.user.id,
        )).options(joinedload_all("tags.tag")).one()
        g.redis.set("searcher:%s:character_id" % searcher_id, character.id)
        character_tags = ["%s:%s" % (_.tag.type, _.tag.name) for _ in character.tags]
        if len(character_tags) != 0:
            g.redis.sadd("searcher:%s:character_tags" % searcher_id, *character_tags)
    except (ValueError, NoResultFound):
        g.redis.delete("searcher:%s:character_id" % searcher_id)

    # Misc tags are symmetrical so we put them in character_tags too.
    misc = [_ for _ in search_tags if _.startswith("misc:")]
    if len(misc) != 0:
        g.redis.sadd("searcher:%s:character_tags" % searcher_id, *misc)

    g.redis.expire("searcher:%s:session_id" % searcher_id, 30)
    g.redis.expire("searcher:%s:search_tags" % searcher_id, 30)
    g.redis.expire("searcher:%s:character_id" % searcher_id, 30)
    g.redis.expire("searcher:%s:character_tags" % searcher_id, 30)

    return jsonify({ "id": searcher_id })


def search_continue():

    searcher_id = request.form["id"][:36]
    cached_session_id = g.redis.get("searcher:%s:session_id" % searcher_id)

    # Send people back to /search if we don't have their data cached.
    if g.user_id is None or cached_session_id != g.session_id:
        abort(404)

    g.redis.expire("searcher:%s:session_id" % searcher_id, 30)
    g.redis.expire("searcher:%s:search_tags" % searcher_id, 30)
    g.redis.expire("searcher:%s:character_id" % searcher_id, 30)
    g.redis.expire("searcher:%s:character_tags" % searcher_id, 30)

    pubsub = g.redis.pubsub()
    pubsub.subscribe("searcher:%s" % searcher_id)

    g.redis.sadd("searchers", searcher_id)

    for msg in pubsub.listen():
        if msg["type"] == "message":
            # The pubsub channel sends us a JSON string, so we return that
            # instead of using jsonify.
            resp = make_response(msg["data"])
            resp.headers["Content-type"] = "application/json"
            return resp


@use_db
@log_in_required
def search_stop():
    searcher_id = request.form["id"][:36]
    g.redis.srem("searchers", searcher_id)
    # Kill the long poll request.
    g.redis.publish("searcher:%s" % searcher_id, "{\"status\":\"quit\"}")
    return "", 204

