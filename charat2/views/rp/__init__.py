import os, datetime
from flask import g, jsonify, render_template, request, redirect, url_for

from charat2.helpers import alt_formats
from charat2.helpers.auth import log_in_required
from charat2.model import GroupChat
from charat2.model.connections import use_db

@use_db
def home():
    return render_template(
        "rp/home.html",
    )

@alt_formats(set(["json"]))
@use_db
def rooms(fmt=None):
    if g.user is not None:
        rooms_query = g.db.query(GroupChat).filter(GroupChat.publicity.in_(("listed", "pinned")))
        rooms = [(_, len(set(g.redis.hvals("chat:%s:online" % _.id)))) for _ in rooms_query]
        rooms.sort(key=lambda _: (_[0].publicity, _[1]), reverse=True)
        chat_dicts = []
        for chat, online in rooms:
            cd = chat.to_dict()
            cd["online"] = online
            chat_dicts.append(cd)
        if fmt == "json":
            r = jsonify({
                "chats": chat_dicts,
            })
            r.headers.add('Last-Modified', datetime.datetime.now())
            r.headers.add('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0')
            r.headers.add('Pragma', 'no-cache')
            return r

        return render_template("rp/rooms.html", rooms=chat_dicts)
    else:
        return redirect(url_for("log_in"))
