import datetime

from datetime import timedelta
from flask import Flask, abort, current_app, g, jsonify, redirect, render_template, request, url_for, make_response
from functools import wraps
from math import ceil
from sqlalchemy import and_, func
from sqlalchemy.orm import joinedload, joinedload_all
from sqlalchemy.orm.exc import NoResultFound
from webhelpers import paginate

from charat2.helpers import alt_formats
from charat2.helpers.auth import log_in_required
from charat2.helpers.chat import UnauthorizedException, BannedException, TooManyPeopleException, authorize_joining, send_message
from charat2.model import (
    case_options,
    AnyChat,
    Ban,
    Character,
    Chat,
    ChatUser,
    GroupChat,
    Invite,
    Message,
    PMChat,
    User,
)
from charat2.model.connections import use_db
from charat2.model.validators import url_validator

import time
from functools import update_wrapper

from redis import Redis
redis = Redis()

class RateLimit(object):
    expiration_window = 10

    def __init__(self, key_prefix, limit, per, send_x_headers):
        self.reset = (int(time.time()) // per) * per + per
        self.key = key_prefix + str(self.reset)
        self.limit = limit
        self.per = per
        self.send_x_headers = send_x_headers
        p = redis.pipeline()
        p.incr(self.key)
        p.expireat(self.key, self.reset + self.expiration_window)
        self.current = min(p.execute()[1], limit)

    remaining = property(lambda x: x.limit + x.current)
    over_limit = property(lambda x: x.current >= x.limit)

def get_view_rate_limit():
    return getattr(g, '_view_rate_limit', None)

def on_over_limit(limit):
    return 'You hit the rate limit', 400

def ratelimit(limit, per=300, send_x_headers=True,
              over_limit=on_over_limit,
              scope_func=lambda: request.remote_addr,
              key_func=lambda: request.endpoint):
    def decorator(f):
        def rate_limited(*args, **kwargs):
            key = 'rate-limit/%s/%s/' % (key_func(), scope_func())
            rlimit = RateLimit(key, limit, per, send_x_headers)
            g._view_rate_limit = rlimit
            if over_limit is not None and rlimit.over_limit:
                return over_limit(rlimit)
            return f(*args, **kwargs)
        return update_wrapper(rate_limited, f)
    return decorator

def get_chat(f):
    @wraps(f)
    def decorated_function(url, fmt=None, *args, **kwargs):

        # Helper for doing some special URL stuff with PM chats.
        # Normally we just query for a Chat object with the url. However, PM chat
        # URLs take the form "pm/<username>", so we have to look up the username,
        # find the User it belongs to, and use our URL and theirs to create a
        # special URL.

        if url == "pm":
            abort(404)

        elif url.startswith("pm/"):

            username = url[3:]
            if username == "":
                abort(404)

            # You can't PM yourself.
            if g.user is None or username.lower() == g.user.username.lower():
                abort(404)

            try:
                pm_user = g.db.query(User).filter(
                    func.lower(User.username) == username.lower()
                ).one()
            except NoResultFound:
                abort(404)

            # Fix case if necessary.
            if pm_user.username != username:
                if request.method != "GET":
                    abort(404)
                return redirect(url_for(request.endpoint, url="pm/" + pm_user.username, fmt=fmt))

            # Generate URL from our user ID and their user ID.
            # Sort so they're always in the same order.
            pm_url = "pm/" + ("/".join(sorted([str(g.user.id), str(pm_user.id)])))
            try:
                chat = g.db.query(PMChat).filter(
                    PMChat.url == pm_url,
                ).one()
            except NoResultFound:
                # Only create a new PMChat on the main chat page.
                if request.endpoint != "rp_chat":
                    abort(404)
                chat = PMChat(url=pm_url)
                g.db.add(chat)
                g.db.flush()
                # Create ChatUser for the other user.
                pm_chat_user = ChatUser.from_user(pm_user, chat_id=chat.id)
                g.db.add(pm_chat_user)

            return f(chat, pm_user, url, fmt, *args, **kwargs)

        # Force lower case.
        if url != url.lower():
            if request.method != "GET":
                abort(404)
            return redirect(url_for(request.endpoint, url=url.lower(), fmt=fmt))

        try:
            chat = g.db.query(AnyChat).filter(AnyChat.url == url).one()
        except NoResultFound:
            abort(404)

        g.chat = chat
        g.chat_id = chat.id
        try:
            authorize_joining(g.redis, g.db, g)
        except BannedException:
            if request.endpoint != "rp_chat" or chat.url == "theoubliette":
                abort(403)
            if request.method != "GET":
                abort(404)
            return redirect(url_for(request.endpoint, url="theoubliette", fmt=fmt))
        except UnauthorizedException:
            abort(403)
        except TooManyPeopleException:
            if request.endpoint == "rp_chat":
                abort(403)

        return f(chat, None, url, fmt, *args, **kwargs)

    return decorated_function


@use_db
@log_in_required
@ratelimit(limit=2, per=60 * 15)
def create_chat():

    # Silently truncate to 50 because we've got a maxlength on the <input>.
    url = request.form["url"][:50]

    # Change URL to lower case but remember the original case for the title.
    lower_url = url.lower()

    if url_validator.match(lower_url) is None:
        return redirect(url_for("rp_rooms", create_chat_error="url_invalid"))

    title = url.replace("_", " ").strip()
    # Don't allow titles to consist entirely of spaces.
    if len(title) == 0:
        title = lower_url

    # Don't allow the URL to collide with the chat_api route
    if lower_url == "chat_api":
        return redirect(url_for("rp_rooms", create_chat_error="url_taken"))
        
    # Check the URL against the routing to make sure it doesn't crash into any
    # of the other routes.
    route, args = current_app.url_map.bind("").match("/" + lower_url)
    if route != "rp_chat":
        return redirect(url_for("rp_rooms", create_chat_error="url_taken"))

    # Don't allow pm because subchats of it (pm/*) will crash into private
    # chat URLs.
    if url == "pm" or g.db.query(Chat.id).filter(Chat.url == lower_url).count() != 0:
        return redirect(url_for("rp_rooms", create_chat_error="url_taken"))

    g.db.add(GroupChat(
        url=lower_url,
        title=title,
        creator_id=g.user.id,
    ))
    
    return redirect(url_for("rp_chat", url=lower_url))


# XXX CUSTOM LOG IN/REGISTER PAGE WITH CHAT INFO
@alt_formats({"json"})
@use_db
@log_in_required
@get_chat
def chat(chat, pm_user, url, fmt=None):

    chat_dict = chat.to_dict()

    if chat.type == "pm":
        # Override title with the other person's username.
        chat_dict['title'] = "Messaging " + pm_user.username
        chat_dict['url'] = url

    if chat.type == "request" or chat.type == "random":
        # Override title with url
        chat_dict['title'] = url

    # Get or create ChatUser.
    try:
        chat_user = g.db.query(ChatUser).filter(and_(
            ChatUser.user_id == g.user.id,
            ChatUser.chat_id == chat.id,
        )).one()
    except NoResultFound:
        chat_user = ChatUser.from_user(g.user, chat_id=chat.id)
        if chat.type == "group":
            if g.user.id != chat.creator_id:
                chat_user.subscribed = False
            if chat.autosilence and g.user.group != "admin" and g.user.id != chat.creator_id:
                chat_user.group = "silent"
        g.db.add(chat_user)
        g.db.flush()

    # Show the last 50 messages.
    messages = g.db.query(Message).filter(
        Message.chat_id == chat.id,
    ).options(joinedload(Message.user)).order_by(
        Message.posted.desc(),
    ).limit(50).all()
    messages.reverse()

    latest_num = messages[-1].id if len(messages) > 0 else 0

    if fmt == "json":

        r = jsonify({
            "chat": chat_dict,
            "chat_user": chat_user.to_dict(include_options=True),
            "messages": [
                _.to_dict() for _ in messages
            ],
            "latest_num": latest_num,
        })
        r.headers.add('Last-Modified', datetime.datetime.now())
        r.headers.add('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0')
        r.headers.add('Pragma', 'no-cache')
        return r

    # Character info for settings form.
    characters = g.db.query(Character).filter(Character.user_id == g.user.id).order_by(Character.title).all()

    r = make_response(render_template(
        "rp/chat.html",
        url=url,
        chat=chat_dict,
        chat_user=chat_user,
        chat_user_dict=chat_user.to_dict(include_options=True),
        messages=messages,
        latest_num=latest_num,
        case_options=case_options,
        characters=characters,
    ))
    r.headers['Last-Modified'] = datetime.datetime.now()
    r.headers['Cache-Control'] = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0'
    r.headers ['Pragma'] = 'no-cache'

    return r


@use_db
@get_chat
def log(chat, pm_user, url, fmt=None, page=None):

    chat_dict = chat.to_dict()

    if chat.type == "pm":
        # Override title with the other person's username.
        chat_dict['title'] = "Log with " + pm_user.username
        chat_dict['url'] = url
        
    # Get or create ChatUser.
    try:
        chat_user = g.db.query(ChatUser).filter(and_(
            ChatUser.user_id == g.user.id,
            ChatUser.chat_id == chat.id,
        )).one()
    except NoResultFound:
        chat_user = ChatUser.from_user(g.user, chat_id=chat.id)
        if chat.type == "group":
            if g.user.id != chat.creator_id:
                chat_user.subscribed = False
            if chat.autosilence and g.user.group != "admin" and g.user.id != chat.creator_id:
                chat_user.group = "silent"
        g.db.add(chat_user)
        g.db.flush()

    message_count = g.db.query(func.count('*')).select_from(Message).filter(
        Message.chat_id == chat.id,
    ).scalar()

    messages_per_page = 200

    if page is None:
        # Default to last page.
        page = int(ceil(float(message_count) / messages_per_page))
        # The previous calculation doesn't work if pages have no messages.
        if page < 1:
            page = 1

    messages = g.db.query(Message).filter(
        Message.chat_id == chat.id,
    ).order_by(Message.id).limit(messages_per_page).offset((page - 1) * messages_per_page).all()

    if len(messages) == 0 and page != 1:
        return redirect(url_for("rp_log", url=url, fmt=fmt))

    if fmt == "json":

        r = jsonify({
            "total": message_count,
            "messages": [_.to_dict() for _ in messages],
        })
        r.headers.add('Last-Modified', datetime.datetime.now())
        r.headers.add('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0')
        r.headers.add('Pragma', 'no-cache')
        return r

    paginator = paginate.Page(
        [],
        page=page,
        items_per_page=messages_per_page,
        item_count=message_count,
        url=lambda page: url_for("rp_log", url=url, page=page),
    )

    return render_template(
        "rp/log.html",
        url=url,
        chat=chat,
        chat_user_dict=chat_user.to_dict(include_options=True),
        messages=messages,
        paginator=paginator,
    )


@alt_formats({"json"})
@use_db
@log_in_required
@get_chat
def users(chat, pm_user, url, fmt=None, page=1):

    if chat.type != "group":
        abort(404)

    try:
        own_chat_user = g.db.query(ChatUser).filter(and_(
            ChatUser.chat_id == chat.id,
            ChatUser.user_id == g.user.id,
        )).one()
    except:
        own_chat_user = None

    user_count = g.db.query(func.count('*')).select_from(ChatUser).filter(
        ChatUser.chat_id == chat.id,
    ).scalar()

    users = g.db.query(ChatUser, User).join(
        User, ChatUser.user_id == User.id,
    ).filter(
        ChatUser.chat_id == chat.id,
    ).options(
        joinedload_all("invite.creator"),
        joinedload_all("ban.creator"),
    ).order_by(User.username).limit(20).offset((page - 1) * 20).all()

    if len(users) == 0:
        abort(404)

    if fmt == "json":
        r = jsonify({ "users": [_[0].to_dict() for _ in users] })
        r.headers.add('Last-Modified', datetime.datetime.now())
        r.headers.add('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0')
        r.headers.add('Pragma', 'no-cache')
        return r

    paginator = paginate.Page(
        [],
        page=page,
        items_per_page=20,
        item_count=user_count,
        url=lambda page: url_for("rp_users", url=url, page=page),
    )

    return render_template(
        "rp/chat_users.html",
        chat=chat,
        own_chat_user=own_chat_user,
        users=users,
        paginator=paginator,
    )


@alt_formats({"json"})
@use_db
@log_in_required
@get_chat
def invites(chat, pm_user, url, fmt=None, page=1):

    if chat.type != "group" or chat.publicity != "private":
        abort(404)

    try:
        own_chat_user = g.db.query(ChatUser).filter(and_(
            ChatUser.chat_id == chat.id,
            ChatUser.user_id == g.user.id,
        )).one()
    except NoResultFound:
        abort(404)

    if not own_chat_user.can("invite"):
        abort(403)

    invite_count = g.db.query(func.count('*')).select_from(Invite).filter(
        Invite.chat_id == chat.id,
    ).scalar()

    invites = g.db.query(Invite, User).filter(
        Invite.chat_id == chat.id,
    ).join(Invite.user).options(
        joinedload(Invite.chat_user),
        joinedload(Invite.creator_chat_user),
    ).order_by(User.username).limit(20).offset((page - 1) * 20).all()

    if len(invites) == 0 and page != 1:
        abort(404)

    if fmt == "json":
        return jsonify({
            "total": invite_count,
            "invites": [invite.to_dict() for invite, user in invites],
        })

    paginator = paginate.Page(
        [],
        page=page,
        items_per_page=20,
        item_count=invite_count,
        url=lambda page: url_for("rp_invites", url=url, page=page),
    )

    return render_template(
        "rp/chat_invites.html",
        chat=chat,
        own_chat_user=own_chat_user,
        invites=invites,
        paginator=paginator,
    )


@use_db
@log_in_required
@get_chat
def invite(chat, pm_user, url, fmt):

    if request.form["username"] == g.user.username:
        if (
            "X-Requested-With" in request.headers
            and request.headers["X-Requested-With"] == "XMLHttpRequest"
        ):
            abort(404)
        return redirect((
            request.headers.get("Referer") or url_for("rp_invites", url=url)
        ).split("?")[0] + "?invite_error=yourself")

    if chat.type != "group" or chat.publicity != "private":
        abort(404)

    try:
        own_chat_user = g.db.query(ChatUser).filter(and_(
            ChatUser.chat_id == chat.id,
            ChatUser.user_id == g.user.id,
        )).one()
    except NoResultFound:
        abort(404)

    if not own_chat_user.can("invite"):
        abort(403)

    try:
        invite_user = g.db.query(User).filter(
            func.lower(User.username) == request.form["username"].lower(),
        ).one()
    except NoResultFound:
        if (
            "X-Requested-With" in request.headers
            and request.headers["X-Requested-With"] == "XMLHttpRequest"
        ):
            abort(404)
        return redirect((
            request.headers.get("Referer") or url_for("rp_invites", url=url)
        ).split("?")[0] + "?invite_error=no_user")

    if g.db.query(func.count("*")).select_from(Invite).filter(and_(
        Invite.chat_id == chat.id, Invite.user_id == invite_user.id,
    )).scalar() == 0:
        g.db.add(Invite(chat_id=chat.id, user_id=invite_user.id, creator_id=g.user.id))
        # Subscribe them to the chat and make it unread so they get a notification about it.
        try:
            invite_chat_user = g.db.query(ChatUser).filter(and_(
                ChatUser.chat_id == chat.id, ChatUser.user_id == invite_user.id,
            )).one()
        except NoResultFound:
            invite_chat_user = ChatUser.from_user(user=invite_user, chat_id=chat.id)
            # Disable typing notifications by default in group chats.
            invite_chat_user.typing_notifications = False
            g.db.add(invite_chat_user)
        invite_chat_user.subscribed = True
        invite_chat_user.last_online = chat.last_message - timedelta(0, 1)
        send_message(g.db, g.redis, Message(
            chat_id=chat.id,
            user_id=invite_user.id,
            type="user_action",
            name=own_chat_user.name,
            text="%s [%s] invited %s to the chat." % (
                own_chat_user.name, own_chat_user.alias,
                invite_user.username,
            ),
        ))

    if (
        "X-Requested-With" in request.headers
        and request.headers["X-Requested-With"] == "XMLHttpRequest"
    ):
        return "", 204

    if "Referer" in request.headers:
        return redirect(request.headers["Referer"].split("?")[0])
    return redirect(url_for("rp_chat_invites", url=url))


@use_db
@log_in_required
@get_chat
def uninvite(chat, pm_user, url, fmt):

    if request.form["username"] == g.user.username:
        abort(404)

    if chat.type != "group" or chat.publicity != "private":
        abort(404)

    try:
        own_chat_user = g.db.query(ChatUser).filter(and_(
            ChatUser.chat_id == chat.id,
            ChatUser.user_id == g.user.id,
        )).one()
    except NoResultFound:
        abort(404)

    if not own_chat_user.can("invite"):
        abort(403)

    try:
        invite_user = g.db.query(User).filter(
            func.lower(User.username) == request.form["username"].lower(),
        ).one()
    except NoResultFound:
        abort(404)

    if g.db.query(func.count("*")).select_from(Invite).filter(and_(
        Invite.chat_id == chat.id, Invite.user_id == invite_user.id,
    )).scalar() != 0:
        g.db.query(Invite).filter(and_(
           Invite.chat_id == chat.id, Invite.user_id == invite_user.id,
        )).delete()
        # Unsubscribing is impossible if they don't have access to the chat, so
        # we need to force-unsubscribe them here.
        try:
            invite_chat_user = g.db.query(ChatUser).filter(and_(
                ChatUser.chat_id == chat.id, ChatUser.user_id == invite_user.id,
            )).one()
            invite_chat_user.subscribed = False
        except NoResultFound:
            pass
        send_message(g.db, g.redis, Message(
            chat_id=chat.id,
            user_id=invite_user.id,
            type="user_action",
            name=own_chat_user.name,
            text="%s [%s] un-invited %s from the chat." % (
                own_chat_user.name, own_chat_user.alias,
                invite_user.username,
            ),
        ))

    if (
        "X-Requested-With" in request.headers
        and request.headers["X-Requested-With"] == "XMLHttpRequest"
    ):
        return "", 204

    if "Referer" in request.headers:
        return redirect(request.headers["Referer"].split("?")[0])
    return redirect(url_for("rp_chat_invites", url=url))


@use_db
@log_in_required
@get_chat
def unban(chat, pm_user, url, fmt):

    if chat.type != "group":
        abort(404)

    try:
        own_chat_user = g.db.query(ChatUser).filter(and_(
            ChatUser.chat_id == chat.id,
            ChatUser.user_id == g.user.id,
        )).one()
    except NoResultFound:
        abort(404)

    if not own_chat_user.can("ban"):
        abort(403)

    # Fetch the ChatUser we're trying to unban.
    if "user_id" in request.form:
        user_condition = ChatUser.user_id == request.form["user_id"]
    elif "username" in request.form:
        user_condition = func.lower(User.username) == request.form["username"].lower()
    else:
        abort(400)
    try:
        unban_chat_user, unban_user = g.db.query(ChatUser, User).join(
            User, ChatUser.user_id == User.id,
        ).filter(and_(
            user_condition,
            ChatUser.chat_id == g.chat.id,
        )).one()
    except NoResultFound:
        abort(404)

    try:
        ban = g.db.query(Ban).filter(and_(Ban.chat_id == chat.id, Ban.user_id == unban_chat_user.user_id)).one()
    except NoResultFound:
        abort(404)

    g.db.delete(ban)

    send_message(g.db, g.redis, Message(
        chat_id=chat.id,
        user_id=unban_chat_user.user_id,
        type="user_action",
        name=own_chat_user.name,
        text=(
            "[color=#%s]%s[/color] unbanned "
            "[color=#%s]%s[/color] from the chat."
        ) % (
            own_chat_user.color,
            g.user.username,
            unban_chat_user.color,
            unban_user.username,
        ),
    ))

    if "Referer" in request.headers:
        return redirect(request.headers["Referer"])
    return redirect(url_for("rp_chat_users", url=url))


def _alter_subscription(chat, pm_user, url, new_value):

    try:
        chat_user = g.db.query(ChatUser).filter(and_(
            ChatUser.user_id == g.user.id,
            ChatUser.chat_id == chat.id,
        )).one()
    except NoResultFound:
        abort(404)

    chat_user.subscribed = new_value

    if "X-Requested-With" in request.headers and request.headers["X-Requested-With"] == "XMLHttpRequest":
        return "", 204

    if "Referer" in request.headers:
        return redirect(request.headers["Referer"])
    return redirect(url_for("rp_chat", url=url))


@use_db
@log_in_required
@get_chat
def subscribe(chat, pm_user, url, fmt):
    return _alter_subscription(chat, pm_user, url, True)


@use_db
@log_in_required
@get_chat
def unsubscribe(chat, pm_user, url, fmt):
    return _alter_subscription(chat, pm_user, url, False)

