from bcrypt import gensalt, hashpw
from flask import  g, jsonify, render_template, redirect, request, url_for, flash
from sqlalchemy import func
from sqlalchemy.orm.exc import NoResultFound
from urlparse import urlparse

from charat2.helpers import alt_formats
from charat2.model import User
from charat2.model.connections import use_db
from charat2.model.validators import username_validator, reserved_usernames
from charat2.views.root import generate_confirmation_token, confirm_token, send_email

import requests

from itsdangerous import URLSafeTimedSerializer

import datetime

def referer_or_home():
    if "Referer" in request.headers:
        r = urlparse(request.headers["Referer"])
        return r.scheme + "://" + r.netloc + r.path
    return url_for("home")


@use_db
def log_in_get():
    return render_template("root/log_in.html")


@alt_formats({"json"})
@use_db
def log_in_post(fmt=None):

    # Check username, lowercase to make it case-insensitive.
    try:
        user = g.db.query(User).filter(
            func.lower(User.username) == request.form["username"].lower()
        ).one()
    except NoResultFound:
        if fmt == "json":
            return jsonify({"error": "no_user"}), 400
        return redirect(referer_or_home() + "?log_in_error=no_user")

    # Check password.
    if hashpw(
        request.form["password"].encode("utf8"),
        user.password.encode()
    ) != user.password:
        if fmt == "json":
            return jsonify({"error": "wrong_password"}), 400
        return redirect(referer_or_home() + "?log_in_error=wrong_password")

    g.redis.set("session:" + g.session_id, user.id)
 
    if fmt == "json":
        return jsonify(user.to_dict(include_options=True))

    redirect_url = referer_or_home()
    # Make sure we don't go back to the log in page.
    if redirect_url == url_for("log_in", _external=True):
        return redirect(url_for("home"))
    return redirect(redirect_url)


def log_out():
    if "session" in request.cookies:
        g.redis.delete("session:" + request.cookies["session"])
    return redirect(referer_or_home())


def register_get():
    return render_template("root/register.html")


@use_db
def register_post():
    r = requests.post('https://www.google.com/recaptcha/api/siteverify', data = {'secret':"6Ley0CATAAAAAF2vfm01am_GgTdl-5gLL2EU0m5r", 'response':request.form["g-recaptcha-response"]})
    
    if r.json()['success']:
        # Don't accept blank fields.
        if request.form["username"] == "" or request.form["password"] == "" or request.form["email"] == "":
            return redirect(referer_or_home() + "?register_error=blank")

        # Make sure the two passwords match.
        if request.form["password"] != request.form["password_again"]:
            return redirect(referer_or_home() + "?register_error=passwords_didnt_match")

        # Check username against username_validator.
        # Silently truncate it because the only way it can be longer is if they've hacked the front end.
        username = request.form["username"][:50]
        if username_validator.match(username) is None:
            return redirect(referer_or_home() + "?register_error=invalid_username")

        # XXX DON'T ALLOW USERNAMES STARTING WITH GUEST_.
        # Make sure this username hasn't been taken before.
        # Also check against reserved usernames.
        existing_username = g.db.query(User.id).filter(
            func.lower(User.username) == username.lower()
        ).count()
        if existing_username == 1 or username.lower() in reserved_usernames:
            return redirect(referer_or_home() + "?register_error=username_taken")
        
        # Email Validation
        email = request.form["email"]
        existing_email = g.db.query(User.id).filter(
            User.email == email
        ).count()
        if existing_email == 1:
            return redirect(referer_or_home() + "?register_error=email_taken")
            
        if request.form["nsfw_access"] == "true":
            nsfw_access = True
        else:
            nsfw_access = False

        new_user = User(
            username=username,
            password=hashpw(request.form["password"].encode("utf8"), gensalt()),
            email=request.form["email"],
            group="guest",
            nsfw_access=nsfw_access,
            registered_on=datetime.datetime.now(),
            last_ip=request.headers["X-Forwarded-For"],
        )
        
        g.db.add(new_user)
        g.db.flush()
        g.redis.set("session:" + g.session_id, new_user.id)
        g.db.commit()
        
        token = generate_confirmation_token(new_user.email)
        confirm_url = url_for('confirm_email', token=token, _external=True)
        html = render_template('email_confirmation.html', confirm_url=confirm_url)
        subject = "Please confirm your email!"
        send_email(new_user.email, subject, html)

        redirect_url = referer_or_home()
        # Make sure we don't go back to the log in page.
        if redirect_url == url_for("register", _external=True):
            return redirect(url_for("home"))
        return redirect(redirect_url)
    else:
        return redirect(referer_or_home() + "?register_error=recaptcha_error")
    
@use_db
def confirm_email(token):
    email = ""
    try:
        email = confirm_token(token)
    except:
        flash('The confirmation link is invalid or has expired. If you are experiencing issues, please email help@charatrp.com.', 'danger')
        return redirect(url_for("home"))
    try:
        user = g.db.query(User).filter(User.email==email).one()
    except:
        flash('The confirmation link is invalid or has expired. If you are experiencing issues, please email help@charatrp.com.', 'danger')
        return redirect(url_for("home"))
    try:
        if user.confirmed:
            flash('Account already confirmed. Please login.', 'success')
            return redirect(url_for("home"))
        else:
            user.confirmed = True
            user.confirmed_on = datetime.datetime.now()
            user.group = 'active'
            g.db.add(user)
            g.db.commit()
            flash('You have confirmed your account. Thanks! If you are experiencing issues, please email help@charatrp.com.', 'success')
    except:
        flash('You have confirmed your account. Thanks!', 'success')
    return redirect(url_for('home'))
