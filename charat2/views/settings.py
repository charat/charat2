from flask import g, jsonify, redirect, render_template, request, url_for
from sqlalchemy import and_
from sqlalchemy.orm import joinedload
from sqlalchemy.orm.exc import NoResultFound

from charat2.helpers.auth import log_in_required
from charat2.model.connections import use_db
from charat2.model import User

options = {
    "desktop_notifications" : "Desktop Notifications",
    "show_description" : "Show Topic",
    "show_connection_messages" : "Connection Messages",
    "show_ic_messages" : "Show IC",
    "show_ooc_messages" : "Show OOC",
    "show_message_info" : "Show Message Info",
    "show_preview" : "Show Preview",
    "typing_notifications" : "Typing Notifications",
    "sound_notifications" : "Sound Notifications",
    "nsfw_access" : "NSFW Access"
}

@use_db
@log_in_required
def settings_get():
    return render_template(
        "rp/settings.html",
        options=options,
    )
    
@use_db
@log_in_required
def settings_post():
    try:
        user = g.db.query(User).filter(User.id == g.user.id).one()
    except NoResultFound:
        abort(404)
    
    for option in options:
        setattr(user, option, option in request.form)
    return redirect(url_for("settings"))
