import json

from flask import Flask, g, render_template, request, redirect, url_for, current_app

from charat2.model.connections import use_db
from charat2.views import blog

from flask.ext.mail import Mail, Message

from itsdangerous import URLSafeTimedSerializer

app = Flask(__name__)
with app.app_context():
    mail = Mail(current_app)
    
    def generate_confirmation_token(email):
        serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
        return serializer.dumps(email, salt=current_app.config['SECURITY_PASSWORD_SALT'])

    def confirm_token(token, expiration=3600):
        serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
        try:
            email = serializer.loads(
                token,
                salt=current_app.config['SECURITY_PASSWORD_SALT'],
                max_age=expiration
            )
        except:
            return False
        return email
        
    def send_email(recipient, subject, body):
        import smtplib

        gmail_user = "help@charatrp.com"
        gmail_pwd = "wexKdplLJ^5AI%xc"
        FROM = gmail_user
        TO = recipient if type(recipient) is list else [recipient]
        SUBJECT = subject
        TEXT = body

        # Prepare actual message
        message = """\From: %s\nTo: %s\nSubject: %s\n\n%s
        """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
        try:
            server = smtplib.SMTP("smtp.gmail.com", 587)
            server.ehlo()
            server.starttls()
            server.login(gmail_user, gmail_pwd)
            server.sendmail(FROM, TO, message)
            server.close()
            print 'successfully sent the mail'
        except:
            print "failed to send mail"