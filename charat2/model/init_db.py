#!/usr/bin/python
import os

from alembic.config import Config
from alembic import command

from charat2.model import Base, engine

if __name__ == "__main__":
    engine.echo = True
    Base.metadata.create_all(bind=engine)
    alembic_cfg = Config(os.path.dirname(os.path.realpath(__file__)) + "/../../alembic.ini")
    command.stamp(alembic_cfg, "head")
