function changeCharacter(char_id) {
	var character = {};
    $.getJSON('/characters/'+char_id+'.json', function (data) {
        character = data;
    }).complete(function () {
        $('#usingname').val(character.name);
        $('#ailin').val(character.alias);
        $('#coln').val(character.color);

        if (character.prefix) {
            $('#prei').val(character.prefix);
        } else {
            $('#prei').val('');
        }

        if (character.suffix) {
            $('#sufi').val(character.suffix);
        } else {
            $('#sufi').val('');
        }
        
        try {
            if (character['case']) {
                $('#casing').val(character['case']);
            } else {
                $('#casing').val('');
            }
        } catch(e) {}

        if (character.replacements && character.replacements.length > 0) {
            clearReplacements();
            for (i in character.replacements) {
                addReplacement();
                replacement = character.replacements[i];
                $($('#replacementList input[name="quirk_from"]')[i]).val(replacement[0]);
                $($('#replacementList input[name="quirk_to"]')[i]).val(replacement[1]);
            }
        } else {
            clearReplacements();
        }

        if (character.regexes && character.regexes.length > 0) {
            clearRegexes();
            for (i in character.regexes) {
                addRegex();
                regex = character.regexes[i];
                $($('#replacementList input[name="regex_from"]')[i]).val(regex[0]);
                $($('#replacementList input[name="regex_to"]')[i]).val(regex[1]);
            }
        } else {
            clearRegexes();
        }

        if (character.tags.character && character.tags.character.length > 0) {
        	var tagged = character.tags.character.map(function(elem){ return elem.name; }).join(', ');
        	$('#tags input[name="character"]').val(tagged);
        } else {
        	$('#tags input[name="character"]').val('');
        }

        if (character.tags.fandom && character.tags.fandom.length > 0) {
        	var tagged = character.tags.fandom.map(function(elem){ return elem.name; }).join(', ');
        	$('#tags input[name="fandom"]').val(tagged);
        } else {
        	$('#tags input[name="fandom"]').val('');
        }

        if (character.tags.gender && character.tags.gender.length > 0) {
        	var tagged = character.tags.gender.map(function(elem){ return elem.name; }).join(', ');
        	$('#tags input[name="gender"]').val(tagged);
        } else {
        	$('#tags input[name="gender"]').val('');
        }
    });
}

$(document).ready(function() {
	if ($('#saved-character').prop('checked')) {
		changeCharacter($('#characters').val());
	}

	if ($('#new-character').prop('checked')) {
		$('#save-to-characters').hide();
	}

	if (!$('input[name="edit_character"]').prop('checked')) {
		$('#edit-options').hide();
	}

	$('#editor-button').on('click', function(e) {
		$('input[name="edit_character"]').prop("checked", true);		
		$('#edit-request').hide();
		$('#edit-character').show();
		$('#edit-options').show();
		e.preventDefault();
	});

	$('#editor-close').on('click', function(e) {
		$('#edit-character').hide();
		$('#edit-request').show();
		e.preventDefault();
	});

	$('#characters').on('change', function(e) {
		if ($('#saved-character').prop('checked')) {
			changeCharacter($('#characters').val());
		}
	});

	$('#saved-character').on('change', function(e) {
		if (this.checked) {
			$('#save-to-characters').show();
			changeCharacter($('#characters').val());
		}
	});

	$('#new-character').on('change', function(e) {
		if (this.checked) {
			$('#save-to-characters').hide();

	        $('#usingname').val("anonymous");
	        $('#ailin').val("??");
	        $('#coln').val("000000");
            $('#prei').val('');
            $('#sufi').val('');
            $('#casing').val('normal');
            clearReplacements();
            clearRegexes();
            $('#tags input[name="character"]').val('');
            $('#tags input[name="fandom"]').val('');
            $('#tags input[name="gender"]').val('');
		}
	});

	$('input[name="edit_character"]').on('change', function() {
		if (this.checked) {
			$('#edit-options').show();
		} else {
			$('#edit-options').hide();
		}
	});

	$('#request_prompt').on('keydown keyup change blur', function() {
		if ($(this).val() != "") {
			$("#prompt-alert").hide();
		} else {
			$("#prompt-alert").show();
		}
	});

	$('#request_scenario').on('keydown keyup change blur', function() {
		if ($(this).val() != "") {
			$("#note-alert").hide();
		} else {
			$("#note-alert").show();
		}
	});

    $('#request-form').on('submit', function(e) {
        if ($('#request_prompt').val() == "" && $('#request_scenario').val() == "") {
            e.preventDefault();
            alert('You need to fill out either the Notes or Prompt');
        }
    });
});