/* FINAL VARIABLES */

var SEARCH_PERIOD = 1;
var PING_PERIOD = 10;

var USER_ACTION_URL = '/chat_api/user_action';
var SET_GROUP_URL = '/chat_api/set_group';
var SET_TOPIC_URL = '/chat_api/set_topic';
var SET_FLAG_URL = '/chat_api/set_flag';

var SAVE_URL = '/chat_api/save';

var CHAT_PING = '/chat_api/ping';
var CHAT_MESSAGES = '/chat_api/messages';
var CHAT_QUIT = '/chat_api/quit';

var CHAT_FLAGS = ['autosilence','publicity','nsfw','typing_notifications'];
var CHAT_FLAG_MAP = {
    'autosilence':true,
    'publicity':'listed',
    'nsfw':true,
    'typing_notifications':true
};

var MOD_GROUPS = ['admin', 'creator', 'mod3', 'mod2', 'mod1'];
var GROUP_RANKS = { 'admin': 6, 'mod3': 5, 'mod2': 4, 'mod1': 3, 'user': 2, 'silent': 1 };
var GROUP_DESCRIPTIONS = {
    'admin': { title: 'Adorable Admin', description: 'Charat Staff' },
    'creator': { title: 'Chat Creator', description: 'Silence, Kick, Ban, Cannot be demodded' },
    'mod3': { title: 'Magical Mod', description: 'Silence, Kick and Ban' },
    'mod2': { title: 'Cute-Cute Mod', description: 'Silence and Kick' },
    'mod1': { title: 'Little Mod', description: 'Silence' },
    'user': { title: 'User', description: '' },
    'silent': { title: 'Silenced', description: '' },
};

var ORIGINAL_TITLE = "Charat";
var CHAT_NAME = chat.title ? chat.title : chat.url;
var CONVERSATION_CONTAINER = '#conversation';
var CONVERSATION_ID = '#convo';
var MISSED_MESSAGE_COUNT_ID = '#exclaim';
var USER_LIST_ID = '#users';

/* VARIABLES */
var line = false;

var chats = [chat.url];

var missed_messages = 0;

var chat_topic = chat.topic;

var status = 'chat';
var user_state = 'online';

var current_sidebar = null;

var hidden, visibilityChange;
if (typeof document.hidden !== "undefined") {
    hidden = "hidden";
    visibilityChange = "visibilitychange";
} else if (typeof document.mozHidden !== "undefined") {
    hidden = "mozHidden";
    visibilityChange = "mozvisibilitychange";
} else if (typeof document.msHidden !== "undefined") {
    hidden = "msHidden";
    visibilityChange = "msvisibilitychange";
} else if (typeof document.webkitHidden !== "undefined") {
    hidden = "webkitHidden";
    visibilityChange = "webkitvisibilitychange";
}

var current_user_array = [];
var user_list = {};

var type_force = '';
var sending_line = '';

var command_error = false;

// Websocket Variables
var messages_method = typeof(WebSocket) != "undefined" ? "websocket" : "long_poll";
var ws;
var ws_works = false;
var ws_connected_time = 0;

// Typing Variables
var typing_timeout
var typing;

/* FUNCTIONS */

function colorChange(r, g, b) {
    $('<style>', { 
        type: 'text/css',
        text:'            body {\
                background-color: rgb('+r+', '+g+', '+b+');\
            }\
\
            #topbar .right.userList .userList, #topbar .right.settings .settings {\
                color: rgb('+lowerRGB(r, 26)+', '+lowerRGB(g, 32)+', '+lowerRGB(b, 26)+');\
            }\
\
            .card, .card .titi, #topbar.chatbar span span {\
               color: rgb('+lowerRGB(r, 93)+', '+lowerRGB(g, 86)+', '+lowerRGB(b, 93)+');\
            }\
\
            input[type="submit"], button, #control-buttons .ooc-button, #control-buttons .me-button {\
                background-color: rgb('+lowerRGB(r, 93)+', '+lowerRGB(g, 86)+', '+lowerRGB(b, 93)+');\
            }\
\
            #control-buttons .ooc-button.active, #control-buttons .me-button.active {\
                background-color: rgb('+lowerRGB(r, 80)+', '+lowerRGB(g, 64)+', '+lowerRGB(b, 80)+');\
            }\
\
            input[type="submit"]:hover, button:hover {\
                background-color: rgb('+lowerRGB(r, 80)+', '+lowerRGB(g, 80)+', '+lowerRGB(b, 80)+');\
            }\
\
            .area-select .selection div {\
                color: rgb('+lowerRGB(r, 57)+', '+lowerRGB(g, 46)+', '+lowerRGB(b, 57)+');\
            }\
\
            .area-select.one .selection .one, .area-select.two .selection .two, .select-bind .arrow, input[type="checkbox"]:checked + .checkbox {\
                background-color: rgb('+lowerRGB(r, 57)+', '+lowerRGB(g, 46)+', '+lowerRGB(b, 57)+');\
            }\
\
            hr, input, textarea, .area-select .area, .area-select .selection, .select-bind, input[type="checkbox"] + .checkbox {\
                border-color: rgb('+lowerRGB(r, 57)+', '+lowerRGB(g, 46)+', '+lowerRGB(b, 57)+');\
            }\
\
            a {\
                color: rgb('+lowerRGB(r, 128)+', '+lowerRGB(g, 112)+', '+lowerRGB(b, 128)+');\
            }\
\
            a:hover {\
                color: rgb('+lowerRGB(r, 80)+', '+lowerRGB(g, 64)+', '+lowerRGB(b, 80)+');\
            }'
    }).addClass('newColor').appendTo('head');
}


function lowerRGB(color, subtract) {
    return color-subtract < 0 ? 0: color-subtract;
}

function colorReset() {
    $('.newColor').remove();
}

function alterMeta(variable, value) {
    user.meta[variable] = value;
    var meta_submit = {};
    meta_submit["chat_id"] = chat.id;
    if (value == true || value == false) {
        meta_submit[variable] = value?"on":"off";
    }
    $.post("/chat_api/save_variables", meta_submit);
}

if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function (str){
        return this.slice(0, str.length) == str;
    };
}

if (typeof String.prototype.endsWith != 'function') {
    String.prototype.endsWith = function (str){
        return this.slice(-str.length) == str;
    };
}

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

function htmlEntitiesEncode(str) {
    return String(str).replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"');
}

function getTimestamp(seconds_from_epoch) {
    var month_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var timestamp = new Date(seconds_from_epoch*1000);
    return month_names[timestamp.getMonth()]+' '+timestamp.getDate()+' '+(timestamp.getHours()==0?'0':'')+timestamp.getHours()+':'+(timestamp.getMinutes()<10?'0':'')+timestamp.getMinutes();
}

function topbarSelect(selector) {
    $(selector).parent().addClass($(selector).prop('class'));
}

function topbarDeselect() {
    $('#topbar .right').prop('class', 'right');
}

function isChecked(id) {
    var checked = $("input[@id=" + id + "]:checked").length;
    if (checked == 0) {
        return false;
    } else {
        return true;
    }
}

function unreadNotifications() {
    $.getJSON('/chats/unread.json', function (data) {
        if (data.total!=0) {
            $('#topbar').addClass('unread');
        } else {
            $('#topbar').removeClass('unread');
        }
    }).complete(function () {
        window.setTimeout(unreadNotifications, 10000);
    });
}

// Change into Character
function switchCharacter(character_id) {
    var actionData = {'chat_id': chat.id, 'character_id': character_id};
    $.post('/chat_api/save_from_character', actionData).complete(updateUser);
}

function updateUser() {
    $.getJSON('/'+chat.url+'.json', function (data) {
        user = data.chat_user;
    }).complete(function () {
        $('#usingname').val(user.character.name);
        $('#ailin').val(user.character.alias);
        $('#coln').val(user.character.color);

        if (user.character.prefix) {
            $('#prei').val(user.character.prefix);
        } else {
            $('#prei').val('');
        }

        if (user.character.suffix) {
            $('#sufi').val(user.character.suffix);
        } else {
            $('#sufi').val('');
        }
        
        try {
            if (user.character['case']) {
                $('#casing').val(user.character['case']);
            } else {
                $('#casing').val('');
            }
        } catch(e) {}

        if (user.character.replacements && user.character.replacements.length > 0) {
            clearReplacements();
            for (i in user.character.replacements) {
                addReplacement();
                replacement = user.character.replacements[i];
                $($('#replacementList input[name="quirk_from"]')[i]).val(replacement[0]);
                $($('#replacementList input[name="quirk_to"]')[i]).val(replacement[1]);
            }
        } else {
            clearReplacements();
        }

        if (user.character.regexes && user.character.regexes.length > 0) {
            clearRegexes();
            for (i in user.character.regexes) {
                addRegex();
                regex = user.character.regexes[i];
                $($('#replacementList input[name="regex_from"]')[i]).val(regex[0]);
                $($('#replacementList input[name="regex_to"]')[i]).val(regex[1]);
            }
        } else {
            clearRegexes();
        }
    });
}

// Chat List Management
/*

function addChat(url) {
    chats.push(url);
    $.getJSON('/'+url+'.json', function (data) {
        $('<div>').html('<h1 class="titi">'+$('<div>').text(data.chat.title).text()+'</h1>').addClass('card').appendTo('#chatListChats').on('click', function () {
            switchChat(url);
        });
    });
}

function switchChat(url) {
    $('#convo').empty();
    messageAjax.abort();
    window.clearTimeout(messageTimeout);
    metaAjax.abort();
    window.clearTimeout(metaTimeout);
    $.getJSON('/'+url+'.json', function (data) {
        //change meta option data
        user = data.chat_user;
        chat = data.chat;
        latest_message_id = data.latest_num;
        History.replaceState({}, "", url);
        document.title = data.chat.title+" - "+ORIGINAL_TITLE;
        messageParse({"messages" : data.messages});
        getMessages(true);
    });
}

*/

// Initialize Chat Page

function startChat() {
    status = "chatting";
    if (user.meta.subscribed) {
        $("#subscription").text("−");
    } else {
        $("#subscription").text("+");
    }
    $(CONVERSATION_CONTAINER).scrollTop($(CONVERSATION_CONTAINER).prop("scrollHeight"));
    if (!$(document.body).hasClass('mobile')) {
        $("#textInput").focus();
    }
    var crom = $(CONVERSATION_CONTAINER).scrollTop()+$(CONVERSATION_CONTAINER).height()+24;
    var den = $(CONVERSATION_CONTAINER).prop("scrollHeight");
    $(window).resize(function (e) {
        var lon = den-crom;
        if (lon <= 50) {
            $(CONVERSATION_CONTAINER).scrollTop($(CONVERSATION_CONTAINER).prop("scrollHeight"));
        }
    });
    msgcont = 0;
    $(CONVERSATION_CONTAINER).removeClass('search');
    $('input, select, button').removeAttr('disabled');
    $('#preview').css('color', '#'+user.character.color);
    
    if ($(document.body).hasClass('mobile')) {
        var current_sidebar = null;
    } else {
        var current_sidebar = "userList";
    }
    if (!current_sidebar) {
        setSidebar(null);
    } else {
        setSidebar(current_sidebar);
    }
    if (messages_method == "websocket") {
        websocketMessages();
    } else {
        getMessages(true);
    }
    pingInterval = window.setTimeout(pingServer, PING_PERIOD*1000);
    updateChatPreview();
}

function atBottom(element) {
    var von = $(element).scrollTop()+$(element).height()+24;
    var don = $(element).prop("scrollHeight");
    var lon = don-von;
    if (lon <= 30){
        return true;
    } else {
      return false;
    }
}

function goBottom(element) {
    $(element).scrollTop($(element).prop("scrollHeight"));
}

function addLine(msg){
    if (msg) {
        var at_bottom = atBottom(CONVERSATION_CONTAINER);

        if ((((msg.type == 'ic' || msg.type == 'me') && user.meta.show_ic_messages) || 
            (msg.type == 'ooc' && user.meta.show_ooc_messages) || 
            ((msg.type == 'disconnect' || msg.type == 'join' || msg.type == 'timeout') && user.meta.show_connection_messages)) && 
            !at_bottom) {
            $(MISSED_MESSAGE_COUNT_ID).html(parseInt($(MISSED_MESSAGE_COUNT_ID).html())+1);
        }

        msg.name = htmlEntities(msg.name);
        msg.alias = htmlEntities(msg.alias);

        msg.original_name = htmlEntitiesEncode(msg.name);
        msg.original_alias = htmlEntitiesEncode(msg.alias);

        message = bbEncode(msg.text);
    
        var alias = "";
        if (msg.alias) {
            alias = msg.alias+":\u00A0";
        }
        if (msg.type == 'ooc') {
            alias = msg.user.username+":\u00A0";
        }
    
        if ($(CONVERSATION_CONTAINER+' p:last').hasClass("user"+msg.user_id) && $(CONVERSATION_CONTAINER+' p:last').hasClass('ic') && msg.type == 'ic') {
            $(CONVERSATION_CONTAINER+' p:last').hide();
        }
    
        var left_text = msg.type;
        if (msg.name) {
            if (msg.type == 'ic') {
                left_text = msg.original_name;
            } else {
                left_text = msg.original_name+':'+msg.type;
            }
        }
        
        var right_text = '<span class="username">';
        if (chat.type == "group") {
            right_text += "<a href='pm/"+msg.user.username+"'>"+msg.user.username+"</a>";
        } else if (chat.type == "pm") {
            right_text += msg.user.username;
        } else if (msg.user.id == user.meta.user_id) {
            right_text = "You";
        }
        
        
        right_text += '</span> <span class="post_timestamp">'+getTimestamp(msg.posted)+'<span>';
        
        var message_container = $('<span>').prop("id","message"+msg.id).addClass(msg.type).addClass("user"+msg.user.id).appendTo(CONVERSATION_ID);
        var info = $('<p>').addClass("info").appendTo("#message"+msg.id);
        var info_left = $('<span>').addClass("left").text(left_text).appendTo("#message"+msg.id+" .info");
        var info_right = $('<span>').addClass("right").html(right_text).appendTo("#message"+msg.id+" .info");
        if (msg.type == 'me') {
            var message = $('<p>').addClass("message").html("<span style=\"color: #"+msg.color+";\">"+msg.name+"</span>"+(msg.alias.length>0?" [<span style=\"color: #"+msg.color+";\">"+msg.alias+"</span>]":"")+" "+message).appendTo("#message"+msg.id);
        } else {
            var message = $('<p>').addClass("message").css('color', '#'+msg.color).html(alias+message).appendTo("#message"+msg.id);
        }

        if (at_bottom) {
            goBottom(CONVERSATION_CONTAINER);
            at_bottom = false;
        }
        
        if ((((msg.type == 'ic' || msg.type == 'me') && user.meta.show_ic_messages) || 
            (msg.type == 'ooc' && user.meta.show_ooc_messages) || 
            ((msg.type == 'disconnect' || msg.type == 'join' || msg.type == 'timeout') && user.meta.show_connection_messages))) {
            if (!document.hasFocus()) {
                missed_messages++;
                if (missed_messages !=0) {
                    document.title = missed_messages+"! "+CHAT_NAME;
                }
                if (user.meta.desktop_notifications && (msg.type == 'ic' || msg.type == 'ooc' || msg.type == 'me')) {
                    desktopNotification(msg.original_name+' - '+chat.title,msg.type!='me'?alias+bbRemoveAll(msg.text):msg.original_name+(msg.original_alias ? ' ['+msg.original_alias+'] ':' ')+bbRemoveAll(msg.text),'http://dev.charat.net/static/img/favicons/rp/touch-icon-ipad.png');
                }
            }

            if (user.meta.sound_notifications) {
                if (!document.hasFocus())
                    AUDIO_NOTIFICATION.play();
            }
        }

        shownotif = 0;
    }
}

function generateUserList(user_data) {
    for (var i=0; i<user_data.length; i++) {
        var list_user = user_data[i];
        var is_self = "";
        var user_description = GROUP_DESCRIPTIONS[list_user.meta.group].title+(GROUP_DESCRIPTIONS[list_user.meta.group].description ? ' – '+GROUP_DESCRIPTIONS[list_user.meta.group].description : '')
        if (list_user.meta.user_id == user.meta.user_id) {
            is_self = " self";
            //updateUser();
            $(USER_LIST_ID).prop('class', list_user.meta.group);
            $("#userList").prop('class', "sidebar "+list_user.meta.group);
        }
        
        if ($('#user'+list_user.meta.user_id).length <= 0) {
            var username_text = '';

            if (chat.type == "group" || chat.type == "pm") {
                username_text = '<span class="username">'+list_user.meta.username+'</span>';
            }
            
            $(USER_LIST_ID).append('<li id="user'+list_user.meta.user_id+'" class="'+list_user.meta.username+' '+list_user.meta.group+is_self+'"><span class="userCharacter'+'"  style="color:#'+list_user.character.color+';" title="'+user_description+'">'+$("<div>").text(list_user.character.name).html()+'</span>'+username_text+'</li>');
            
            if (user.meta.group != 'user' && user.meta.group != 'silent') {
                $('#topicButton').show();
            } else {
                $('#topicButton').hide();
            }

            var user_buttons = '<span class="set">' +
                    '<li class="mod3">Make Magical Mod</li>' +
                    '<li class="mod2">Make Cute-Cute Mod</li>' +
                    '<li class="mod1">Make Little Mod</li>' +
                    '<li class="silent">Silence</li>' +
                    '<li class="unsilent">Unsilence</li>' +
                    '<li class="user">Remove Mod Status</li>' +
                '</span>' +
                '<span class="user_action">' +
                    '<li class="kick">Kick</li>' +
                    '<li class="ban">Ban</li>' +
                '</span>' +
                '<span class="chat_action">' +
                    '<li class="block">Block</li>' +
                    '<li class="highlight">Highlight</li>' +
                '</span>';

            $('#user'+list_user.meta.user_id).append('<ul class="user_buttons"></ul>');
            $('#user'+list_user.meta.user_id+' .user_buttons').append(user_buttons);
            user_list[list_user.meta.user_id] = list_user;
            user_list[list_user.meta.username] = list_user;

            $('.user_buttons').hide();
            $('#user'+list_user.meta.user_id).on('click', function () {
                var buttons_shown = $(this).find('.user_buttons').is(':visible');
                $('.user_buttons').hide();
                if (buttons_shown) {
                    $(this).find('.user_buttons').hide();
                } else {
                    $(this).find('.user_buttons').show();
                }
            });
        } else {
            $('#user'+list_user.meta.user_id).prop('class',list_user.meta.username+' '+list_user.meta.group+is_self);
            $('#user'+list_user.meta.user_id+' .userCharacter').css('color','#'+list_user.character.color).prop('title', user_description).text(list_user.character.name);
        }
        
        $('#user'+list_user.meta.user_id+' .set li').off('click');
        $('#user'+list_user.meta.user_id+' .user_action li').off('click');
        
        $('#user'+list_user.meta.user_id+' .set li').on('click', function () {
            var set_group = $(this).attr('class');
            if (set_group == 'unsilent') {
                set_group = 'user';
            }
            setGroup(user_list[$(this).parent().parent().parent().prop('id').substr(4)].meta.username, set_group);
        });
        
        $('#user'+list_user.meta.user_id+' .user_action li').on('click', function () {
            if ($(this).attr('class') != 'ban') {
                userAction(user_list[$(this).parent().parent().parent().prop('id').substr(4)].meta.username, $(this).attr('class'));
            } else {
                $('#textInput').val('/ban '+user_list[$(this).parent().parent().parent().prop('id').substr(4)].meta.username+' <reason>');
            }
        });
        
    }

    $(USER_LIST_ID+" .username").each(function () {
        var in_list = false;
        for (var i=0; i<user_data.length; i++) {
            if ($(this).text() == user_data[i].meta.username) {
                in_list = true;
            }
        }
        if (in_list) {
            $(this).parent().show();
        } else {
            $(this).parent().hide();
        }
    });
}

function userAction(user,action,reason) {
    var actionData = {'chat_id': chat.id, 'action': action, 'username': user, 'reason': reason};
    $.post(USER_ACTION_URL,actionData);
}

function setGroup(user,group) {
    var actionData = {'chat_id': chat.id, 'group': group, 'username': user};
    $.post(SET_GROUP_URL,actionData);
}

function setTopic(topic) {
    var actionData = {'chat_id': chat.id, 'topic': topic};
    $.post(SET_TOPIC_URL,actionData);
}

function setFlag(flag,val) {
    var actionData = {'chat_id': chat.id, 'flag': flag, 'value': val};
    $.post(SET_FLAG_URL,actionData);
}

var messageAjax = null;
var messageTimeout = null

function getMessages(first_join) {
    first_join = (typeof first_join === "undefined") ? false : first_join;

    var messageData = {'chat_id': chat['id'], 'after': latest_message_id};
    if (first_join) {
        sendAlert("Connected!");
        $('#typingNotificationToggle').hide();
        messageData['joining'] = true;
        status = "chatting"
    }

    messageAjax = $.post(CHAT_MESSAGES, messageData, function (data) {
        messageParse(data);
    }, "json").complete(function(jqxhr, text_status) {
        if (status=='chatting') {
            if (jqxhr.status < 400 && text_status == "success") {
                getMessages();
            } else {
                window.setTimeout(getMessages, 2000);
            }
        }
        if (first_join) {
            unreadNotifications();
        }
    });
}

function websocketMessages() {
    if (ws && ws.readyState != 3) { return; }
    status = "connecting";
    ws = new WebSocket("ws://" + location.host + ":8080/" + chat.id + "?after=" + latest_message_id);
    ws.onopen = function(e) {
        sendAlert("Connected!");
        $('#typingNotificationToggle').show();
        $('input, select, button').removeAttr('disabled');
        ws_works = true;
        ws_connected_time = Date.now();
        status = "chatting";
    }
    ws.onmessage = function(e) { messageParse(JSON.parse(e.data)); }
    ws.onclose = function(e) {
        if (status == "connecting" || status == "chatting") {
            // Fall back to long polling if we've never managed to connect.
            if (!ws_works || (Date.now() - ws_connected_time) < 5000) {
                $('input, select, button').removeAttr('disabled');
                messages_method = "long_poll";
                getMessages(true);
                return;
            }
            // Otherwise try to reconnect.
            clearChat();
            sendAlert("Sorry, the connection to the server has been lost. Attempting to reconnect...");
            window.setTimeout(websocketMessages, 2000);
        }
    }
}

function pingServer() {
    if (status == "chatting") {
        if (messages_method == "websocket") {
            if (ws.readyState == 1) {
                    ws.send("ping");
                    window.setTimeout(pingServer, 10000);
            } 
        } else {
            $.post(CHAT_PING, {'chat_id': chat['id']});
            pingInterval = window.setTimeout(pingServer, PING_PERIOD*1000);
            updateChatPreview();
        }
    }
}

function messageParse(data) {
    // KICK/BAN RECEIVAL
    if (typeof data.exit!='undefined') {
        if (data.exit=='kick') {
            clearChat();
            addLine({ counter: -1, color: '000000', text: 'You have been kicked from this chat. Please think long and hard about your behaviour before rejoining.' });
            sendAlert("You have been kicked from this chat. Please think long and hard about your behaviour before rejoining.");
        } else if (data.exit=='ban') {
            clearChat();
            window.location.replace('/theoubliette');
        }
        return true;
    }
    if (typeof data.messages!="undefined") {
        var messages = data.messages;
        for (var i=0; i<messages.length; i++) {
            addLine(messages[i]);
            latest_message_id = Math.max(latest_message_id, messages[i]['id']);
        }
        if (messages.length>0 && typeof hidden!="undefined" && document[hidden]==true) {

        }
    }
    if (typeof data.counter!="undefined") {
        user.meta.counter = data.counter;
    }
    if (typeof data.users!="undefined") {
        generateUserList(data.users);
    }
    if (typeof data.chat!='undefined') {
        // Reload chat metadata.

        chat = data.chat;

        if (data.chat.publicity) {
            $('#publicity option[value="'+data.chat.publicity+'"]').prop('selected', true);
        }
        
        for (i=0; i<CHAT_FLAGS.length; i++) {
            if (data.chat[CHAT_FLAGS[i]]) {
                $('#'+CHAT_FLAGS[i]).prop('checked', 'checked');
                $('#'+CHAT_FLAGS[i]+'Result').show();
            } else {
                $('#'+CHAT_FLAGS[i]).removeAttr('checked');
                $('#'+CHAT_FLAGS[i]+'Result').hide();
            }
        }
        
        if (data.chat.publicity) {
            $('#publicityResult').text('This chat is '+data.chat.publicity+'.');
        }
        
        if (typeof data.chat.topic!='undefined') {
            $('#topic').html(bbEncode(data.chat.topic));
            chat_topic = data.chat.topic;
        } else {
            $('#topic').text('');
            chat_topic = '';
        }

        if (user.meta.group == 'mod3' || user.meta.group == 'admin') {
            $('.inPass').hide();
            $('.editPass').show();
        } else {
            $('.editPass').hide();
            $('.inPass').show();
        }
        if (data.chat['nsfw'] == '1') {
            colorChange(240, 192, 240);
        } else {
            colorReset();
        }
    }
    if (user.meta.group == 'mod3' || user.meta.group == 'creator' || user.meta.group == 'admin') {
        $('.inPass').hide();
        $('.editPass').show();
        $('.opmod input').removeAttr('disabled');
    } else {
        $('.editPass').hide();
        $('.inPass').show();
        $('.opmod input').prop('disabled', 'disabled');
    }
    if (user.meta.typing_notifications == true && typeof data.typing != "undefined") {
        if(data.typing.length == 1) {
            $('#typingWrapper').text(user_list[data.typing[0]].meta.username + " is typing...");
        } else if (data.typing.length > 1) {
            users_typing = data.typing;
            users_typing.forEach(function(value, i){
                users_typing[i] = user_list[value].meta.username;
            });
            var typing_text = users_typing.join(', ');
            typing_text = typing_text.replace(/,([^,]*)$/, " and$1");
            $('#typingWrapper').text(typing_text + " are typing...");
        }
        updateChatPreview();
    }
    if (user.meta.typing_notifications == true && typeof data.typing != "undefined" && data.typing.length == 0) {
        $('#typingWrapper').text('');
    }
}

function sendAlert(alert_text) {
    window.setTimeout(clearAlert, 3000);
    $('#typingWrapper').hide();
    $('#alertWrapper').text(alert_text).show();
}

function clearAlert() {
    $('#typingWrapper').show();
    $('#alertWrapper').text('').hide();
}

function clearChat() {
    status = 'disconnected';
    if (pingInterval) {
        window.clearTimeout(pingInterval);
    }
    $('input[name="chat"]').val(chat.url);
    $('input, select, button').prop('disabled', 'disabled');
    document.title = CHAT_NAME+' – '+ORIGINAL_TITLE;
    msgcont = 0;
}

function setSidebar(sidebar) {
    var at_bottom = atBottom(CONVERSATION_CONTAINER);
    
    $('.sidebar').hide();
    topbarDeselect();
    current_sidebar = sidebar;
    if (current_sidebar) {
        $('#'+current_sidebar).show();
        topbarSelect('#topbar .right .'+current_sidebar);
        $(document.body).addClass('withSidebar');
    } else {
        $(document.body).removeClass('withSidebar');
    }

    if (at_bottom) {
        goBottom(CONVERSATION_CONTAINER);
        at_bottom = false;
    }
}

function closeSettings() {
    if ($(document.body).hasClass('mobile')) {
        if (navigator.userAgent.indexOf('Nintendo 3DS')!=-1 || navigator.userAgent.indexOf('Nintendo DSi')!=-1) {} {
            setSidebar(null);
        }
    } else {
        setSidebar('userList');
    }
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substr(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substr(nameEQ.length,c.length);
    }
    return null;
}

function updateChatPreview() {
    command_error = false;
    $('#aliasOffset').css('top', (5-$('#textInput').scrollTop())+'px');
    var at_bottom = atBottom(CONVERSATION_CONTAINER);
    var textPreview = $('#textInput').val().replace(/\r\n|\r|\n/g,"[br]");
    $('#preview').css('opacity','1');
    $('#preview').css('color', '#'+user.character.color);
    $('#textInput').css('color', '#'+user.character.color);
    $('#aliasOffset').css('color', '#'+user.character.color);
    $('#aliasOffset').css('color','#'+user.character.color);
    if (user.character.alias) {
        $('#aliasOffset').text(user.character.alias+":");
    } else {
        $('#aliasOffset').html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
    }
    $("#textInput").css('text-indent', $('#aliasOffset').width()+4+'px');
    
    var command = $('#textInput').val().split(' ');

    is_command = command[0] == '/ban' || command[0] == '/kick' || command[0] == '/set' || command[0] == '/topic' || command[0] == '/publicity' || command[0] == '/nsfw' || command[0] == '/autosilence' || command[0] == '/typingnotifications' || command[0] == '/me';
    
    if (command[0] == '/' || command[0] == '/ic' || command[0] == '/ooc' || is_command) {
        textPreview = textPreview.substr(command[0].length);
    }

    var textInput = $('#textInput').val();
    if ((textInput.startsWith("/ ") ||
        textInput.startsWith("((") || textInput.endsWith("))") ||
        textInput.startsWith("[[") || textInput.endsWith("]]") ||
        textInput.startsWith("{{") || textInput.endsWith("}}")) ||
        (command[0] == '/me' || command[0] == '/ooc' || command[0] == '/') &&
        command[0] != '/ic') {
        if (command[0] == '/me' || command[0] == '/ooc' || command[0] == '/') {
            textPreview = textPreview.substr(1);
        }
    } else {
        textPreview = command[0] == '/ic' ? textPreview.substr(1) : textPreview;
        if (!user.meta.ooc_on && type_force == '') {
            pattern = user.character;
            user.character.line = line;
            textPreview = applyQuirks(textPreview,pattern);
        }
    }
    
    var aliasPreview = user.character.alias ? user.character.alias+": " : "\xa0";
    
    var textInput = $('#textInput').val();
    if ((!type_force || command[0] == '/ooc') && command[0] != '/me' && command[0] != '/ic' && (command[0] == '/ooc' || user.meta.ooc_on ||
            textInput.startsWith("((") || textInput.endsWith("))") || 
            textInput.startsWith("[[") || textInput.endsWith("]]") || 
            textInput.startsWith("{{") || textInput.endsWith("}}"))) {
        aliasPreview = user.meta.username+": ";
    }
    
    if ((command[0] == '/me' || type_force == 'me') && command[0] != '/ic' && command[0] != '/ooc') {
        $('#preview').css('color', '#000000');
        $('#textInput').css('color','#000000');
        aliasPreview = "[color=#"+user.character.color+"]"+user.character.name+"[/color] "+(user.character.alias?"[[color=#"+user.character.color+"]"+user.character.alias+"[/color]] ":"");
    }

    if (is_command && command[0] != '/me') {
        $('#preview').css('color', '#000000');
        $('#textInput').css('color','#000000');
        aliasPreview = "[color=#"+user.character.color+"]"+user.meta.username+"[/color] ";
    }

    if ((is_command || command[0] == '/ooc' || user.meta.ooc_on || type_force != '') && command[0] != '/ic') {
        if (is_command || command[0] == '/ooc' ) {
            $('#aliasOffset').html($('#aliasOffset').html()+"<span id='commandText'>"+command[0].substr(1)+"</span>");
        } else if (type_force != '') {
            $('#aliasOffset').html($('#aliasOffset').html()+"<span id='commandText'>"+type_force+"</span>");
        } else if (user.meta.ooc_on) {
            $('#aliasOffset').html($('#aliasOffset').html()+"<span id='commandText'>ooc</span>");
        }
    }
    
    if (command[0] == '/ban') {
        try {
            var action_user = user_list[command[1]];
            command.splice(0,2);
            var reason = command.join(" ");
            textPreview = "banned [color=#"+action_user.character.color+"]"+action_user.character.name+"[/color] [[color=#"+action_user.character.color+"]"+action_user.character.alias+"[/color]] from the chat."+(reason ? " Reason: "+reason : "");
        } catch(e) {
            aliasPreview = "";
            textPreview = "[color=#EE0000]Error[/color]";
            command_error = true;
        }
    } else if (command[0] == '/kick') {
        try {
            var action_user = user_list[command[1]];
            textPreview = "kicked [color=#"+action_user.character.color+"]"+action_user.character.name+"[/color] [[color=#"+action_user.character.color+"]"+action_user.character.alias+"[/color]] from the chat.";
        } catch(e) {
            aliasPreview = "";
            textPreview = "[color=#EE0000]Error[/color]";
            command_error = true;
        }
    } else if (command[0] == '/set') {
        var groups = ['magical','cute','little','unsilence','silence'];
        var group_map = {'magical':'mod3', 'cute':'mod2', 'little':'mod1','unsilent':'user','silence':'silent'};
        try {
            if (groups.indexOf(command[2])!=-1 && command[1]) {
                var action_user = user_list[command[1]];
                var group_set = GROUP_DESCRIPTIONS[group_map[command[2]]];
                textPreview = "set [color=#"+action_user.character.color+"]"+action_user.character.name+"[/color]"+(action_user.character.alias?" [[color=#"+action_user.character.color+"]"+action_user.character.alias+"[/color]]": "")+" to "+group_set.title+"."+(group_set.description ? " They can now "+group_set.description+"." : "");
            } else {
                throw "error";
            }
        } catch(e) {
            aliasPreview = "";
            textPreview = "[color=#EE0000]Error[/color]";
            command_error = true;
        }
    } else if (command[0] == '/topic') {
        try {
            command.splice(0,1);
            var new_topic = command.join(" ");
            new_topic = new_topic.replace(/\n/g,'[br]');
            textPreview = "changed the topic to \""+new_topic+"\"";
        } catch(e) {
            aliasPreview = "";
            textPreview = "[color=#EE0000]Error[/color]";
            command_error = true;
        }
    } else if (command[0] == '/nsfw' || command[0] == '/autosilence' || command[0] == '/typingnotifications') {
        try {
            if (command[1] == 'on' || command[1] == 'off') {
                textPreview = "switched "+command[0].substr(1)+" "+command[1];
            } else {
                throw "error";
            }
        } catch(e) {
            aliasPreview = "";
            textPreview = "[color=#EE0000]Error[/color]";
            command_error = true;
        }
    } else if (command[0] == '/publicity') {
        try {
            if (command[1] == 'listed') {
                textPreview = "listed the chat. It's now listed on the public rooms page.";
            } else if (command[1] == 'unlisted') {
                textPreview = "unlisted the chat.";
            } else {
                throw "error";
            }
        } catch(e) {
            aliasPreview = "";
            textPreview = "[color=#EE0000]Error[/color]";
            command_error = true;
        }
    }
    
    if (textPreview.length>0) {
        $('#preview').html(bbEncode(aliasPreview + textPreview));
        sending_line = textPreview;
    } else {
        $('#preview').html(bbEncode(aliasPreview));
    }
    $(CONVERSATION_CONTAINER).css('bottom',($('.controls').height()+20)+'px');
    if(at_bottom) {
        goBottom(CONVERSATION_CONTAINER);
    }
    return textPreview.length!=0;
}

// CHECK IF COOKIES ARE ENABLED

// ADD IN NEW HIGHLIGHTING AND BLOCKING SCRIPTS

$(function (){
    if (document.cookie=="") {
        sendAlert("You cannot chat without Cookies.");
    } else {
        
        try {
            $('#notifications').prop('class', Notification.permission);
        } catch (e) {
            $('#notifications').prop('class', 'denied');
        }
        
        if (user.meta.desktop_notifications) {
            $('#notifications .deskset').prop('checked', 'checked');
        } else {
            $('#notifications .deskset').removeProp('checked');
        }
        
        /* START UP */
        startChat();

        $('.area-select .selection .one').on('click', function(e){
            $(this).parent().parent().removeClass("two").addClass("one");
        });

        $('.area-select .selection .two').on('click', function(e){
            $(this).parent().parent().removeClass("one").addClass("two");
        });

        $('#control-buttons .ooc-button').on('click', function () {
            if (user.meta.ooc_on) {
                if (type_force == 'me') {
                    type_force = '';
                    $('#control-buttons .me-button').removeClass('active');
                    $('#control-buttons .ooc-button').addClass('active');
                } else {
                alterMeta("ooc_on", false);
                    $('#control-buttons .ooc-button').removeClass('active');
                }
            } else {
                if (type_force == 'me') {
                    type_force = '';
                    $('#control-buttons .me-button').removeClass('active');
                }
                alterMeta("ooc_on", true);
                $('#control-buttons .ooc-button').addClass('active');
            }
            updateChatPreview();
        });

        $('#control-buttons .me-button').on('click', function () {
            if (type_force == "me") {
                type_force = '';
                $('#control-buttons .me-button').removeClass('active');
                if (user.meta.ooc_on) {
                    $('#control-buttons .ooc-button').addClass('active');
                } else {
                    $('#control-buttons .ooc-button').removeClass('active');
                }
            } else {
                type_force = 'me';
                $('#control-buttons .me-button').addClass('active');
                $('#control-buttons .ooc-button').removeClass('active');
            }
            updateChatPreview();
        });

        $('#notifications').on('click', function (){
            try {
                if (Notification.permission != 'granted') {
                    Notification.requestPermission(function (e){
                        $('#notifications').prop('class', e);
                        if (e == 'granted') {
                            $('#notifications .deskset').prop('checked', 'checked');
                            alterMeta("desktop_notifications", true);
                        }
                    });
                }
            } catch(e) {}
        });
        
        $('#notifications .deskset').on('click', function (){
            alterMeta("desktop_notifications", $('#notifications .deskset').is(':checked'));
        });
        
        $('#topbar .right span').click(function () {
            if ($(this).prop('class') == current_sidebar) {
                current_sidebar = null;
                setSidebar(current_sidebar);
            } else {
                current_sidebar = $(this).prop('class');
                setSidebar(current_sidebar);
            }
        });
        
        $('#topic').html(bbEncode($('#topic').text()));
        $('#convo span').each(function () {
            var thisLine = bbEncode($(this).find('.message').text());
            $(this).find('.message').html(thisLine);
            $(this).find('.info .right .post_timestamp').text(getTimestamp($(this).find('.info .right .post_timestamp').text()));
        }).promise().done(function () {
            goBottom(CONVERSATION_CONTAINER);
        });

        /* SUBMISSION AND ACTIVE CHANGES */

        // Subscription Changes
        $("#subscription").on('click', function () {
            if (user.meta.subscribed) {
                $.post("/"+chat.url+"/unsubscribe", function() {
                    $("#subscription").text("+");
                    user.meta.subscribed = false;
                });
            } else {
                $.post("/"+chat.url+"/subscribe", function() {
                    $("#subscription").text("−");
                    user.meta.subscribed = true;
                });
            }
        });

        // Hide info if setting is false
        if (!user.meta.show_message_info) {
            $(document.body).addClass('hideInfo');
            goBottom(CONVERSATION_CONTAINER);
        }

        if (!user.meta.show_ooc_messages) {
            $(document.body).addClass('hideOOC');
            goBottom(CONVERSATION_CONTAINER);
        }

        if (!user.meta.show_ic_messages) {
            $(document.body).addClass('hideIC');
            goBottom(CONVERSATION_CONTAINER);
        }

        if (!user.meta.show_connection_messages) {
            $(document.body).addClass('hideConnection');
            goBottom(CONVERSATION_CONTAINER);
        }

        if (user.meta.show_preview) {
            $('#preview').show();
            $('#previewToggle input').prop('checked', true);
        }

        if (user.meta.ooc_on) {
            $('#control-buttons .ooc-button').addClass('active');
        }

        if (!user.meta.show_description) {
            $('#hide-topic .text').text('Show Topic');
            $('#topic').hide();
        }
        
        $('.controls').submit(function () {
            if (command_error) {
                sendAlert('Command Failed');
                $('#textInput').val('');
                updateChatPreview();
                return false;
            }
            $('#textInput').focus();
            if ($.trim($('#textInput').val())=='/ooc') {
                if (!user.meta.ooc_on) {
                    $('.ooc-button').click();
                }
                $('#textInput').val('');
                type_force = '';
                return false;
            } else if ($.trim($('#textInput').val())=='/ic') {
                if (user.meta.ooc_on) {
                    $('.ooc-button').click();
                }
                $('#textInput').val('');
                type_force = '';
                $('#control-buttons .me-button').css('background-color','');
                return false;
            } else if ($.trim($('#textInput').val())=='/me') {
                if (type_force!='me') {
                    $('.me-button').click();
                }
                $('#textInput').val('');
                type_force == 'me';
                return false;
            }


            var createTypeMe = false;
            if (updateChatPreview()) {
                if ($('#textInput').val().charAt(0)=='/') {
                    var command = $('#textInput').val().split(' ');
                    if (command[0] == '/ban') {
                        user = command[1];
                        command.splice(0,2);
                        reason = command.join(" ");
                        userAction(user, 'ban', reason);
                        $('#textInput').val('');
                    }
                    
                    if (command[0] == '/kick') {
                        userAction(command[1],'kick','');
                        $('#textInput').val('');
                    }
                    
                    if (command[0] == '/set') {
                        var groups = ['magical','cute','little','unsilence','silence'];
                        var group_map = {'magical':'mod3', 'cute':'mod2', 'little':'mod1','unsilent':'user','silence':'silent'};
                        if (groups.indexOf(command[2].toLowerCase())!=-1) {
                            setGroup(command[1],group_map[command[2]].toLowerCase());
                        }
                        $('#textInput').val('');
                    }
                    
                    if (command[0] == '/topic') {
                        var com = command;
                        com.splice(0,1);
                        var topic_set = com.join(' ');
                        topic_set = topic_set.replace(/\n/g,'[br]');
                        setTopic(topic_set);
                        $('#textInput').val('');
                    }
                    
                    if (command[0] == '/publicity' || command[0] == '/nsfw' || command[0] == '/autosilence' || command[0] == '/typingnotifications') {
                        setFlag(command[0].substr(1),command[1]);
                        $('#textInput').val('');
                    }
                    
                    if (command[0] == '/ooc' || command[0] == '/ic' || command[0] == '/me') {
                        type_force = command[0].substr(1);
                        command.splice(0,1);
                        $('#preview').text(command.join(' '));
                    }
                }
                
                if ($('#textInput').val().trim()!='') {
                    if (pingInterval) {
                        window.clearTimeout(pingInterval);
                    }
                    var lineSend = sending_line;
                    var type = user.meta.ooc_on ? "ooc" : "ic";
                    var textInput = $('#textInput').val();
                    if (textInput.startsWith("((") || textInput.endsWith("))") || textInput.startsWith("[[") || textInput.endsWith("]]") || textInput.startsWith("{{") || textInput.endsWith("}}")) {
                        type = "ooc";
                    }
                    if (type_force) {
                        type = type_force;
                    }
                    type_force = '';
                    $('#control-buttons .me-button').css('background-color','');
                    $.post('/chat_api/send',{'chat_id': chat['id'], 'text': lineSend, 'type':type});
                    pingInterval = window.setTimeout(pingServer, PING_PERIOD*1000);
                    $('#textInput').val('');
                    line=!line;
                    updateChatPreview();
                }
            }
            $("#textInput").focus();
            $('#textInput').val('');
            if (createTypeMe) {
                $('#control-buttons .me-button').click();
            }

            return false;
        });
        
        $('#topicButton').on('click', function () {
            $('#textInput').val('/topic '+chat_topic);
            updateChatPreview();
            if ($('body.mobile').length>0) {
                $('#topbar .userList').click();
            }
        });

        $('#textInput').change(updateChatPreview).keyup(updateChatPreview).change();

        $("textarea#textInput").on('keydown', function (e) {
            if (messages_method == "websocket" && user.meta.typing_notifications && (chat.type != "group" || chat.typing_notifications == true)) {
                window.clearTimeout(typing_timeout);
                if (!typing) {
                    typing = true;
                    ws.send("typing");
                }
                typing_timeout = window.setTimeout(function() {
                    typing = false;
                    ws.send("stopped_typing");
                }, 1000);
            }

            if (e.keyCode == 13 && !e.shiftKey)
            {
                e.preventDefault();
                $('.controls').submit();
            }
        });

        $('#publicity').on('change', function(e) {
            setFlag('publicity', e.target.value);
            $('#publicity option[value="'+chat.publicity+'"]').prop('selected', true);
        });

        $('#previewToggle input').click(function () {
            alterMeta("show_preview", !user.meta.show_preview);
            if (user.meta.show_preview) {
                $('#preview').show();
            } else {
                $('#preview').hide();
            }
            updateChatPreview();
        });

        $('#soundNotificationToggle input').click(function () {
            alterMeta("sound_notifications", !user.meta.sound_notifications);
        });

        $('#typingNotificationToggle input').click(function () {
            alterMeta("typing_notifications", !user.meta.typing_notifications);
        });

        //key

        $('#showInfoToggle input').click(function () {
            alterMeta("show_message_info", !user.meta.show_message_info);
            if (!user.meta.show_message_info) {
                $(document.body).addClass('hideInfo');
            } else {
                $(document.body).removeClass('hideInfo');
            }
            goBottom(CONVERSATION_CONTAINER);
        });

        $('#showOOCToggle input').click(function () {
            alterMeta("show_ooc_messages", !user.meta.show_ooc_messages);
            if (!user.meta.show_ooc_messages) {
                $(document.body).addClass('hideOOC');
            } else {
                $(document.body).removeClass('hideOOC');
            }
            goBottom(CONVERSATION_CONTAINER);
        });

        $('#showICToggle input').click(function () {
            alterMeta("show_ic_messages", !user.meta.show_ic_messages);
            if (!user.meta.show_ic_messages) {
                $(document.body).addClass('hideIC');
            } else {
                $(document.body).removeClass('hideIC');
            }
            goBottom(CONVERSATION_CONTAINER);
        });

        $('#connectionMSGToggle input').click(function () {
            alterMeta("show_connection_messages", !user.meta.show_connection_messages);
            if (!user.meta.show_connection_messages) {
                $(document.body).addClass('hideConnection');
            } else {
                $(document.body).removeClass('hideConnection');
            }
            goBottom(CONVERSATION_CONTAINER);
        });

        $('#statusButton').click(function () {
            new_state = $('#statusInput').val();
            $('#statusInput').val('');
            $.post(POST_URL, {'chat_id': chat['id'], 'state': new_state}, function (data) {
                user_state = new_state;
            });
        });

        $('#settings').submit(function () {
            formInputs = $('#settings').find('input, select');
            if ($('input[name="name"]').val()=="") {
                sendAlert("You can't chat with a blank name!");
            } else if ($('input[name="color"]').val().match(/^[0-9a-fA-F]{6}$/)==null) {
                sendAlert("You entered an invalid hex code. Try using the color picker.");
            } else {
                var formData = $(this).serializeArray();
                formData.push({ name: 'chat_id', value: chat['id'] })
                $.post(SAVE_URL, formData, function (data) {
                    $('#preview').css('color', '#'+$('input[name="color"]').val());
                    var formInputs = $('#settings').find('input, select');
                    for (i=0; i<formInputs.length; i++) {
                        if (formInputs[i].name!="quirk_from" && formInputs[i].name!="quirk_to") {
                            user.character[formInputs[i].name] = formInputs[i].value;
                        }
                    }
                    user.character.replacements = [];
                    var replacementsFrom = $('#settings').find('input[name="quirk_from"]');
                    var replacementsTo = $('#settings').find('input[name="quirk_to"]');
                    for (i=0; i<replacementsFrom.length; i++) {
                        if (replacementsFrom[i].value!="" && replacementsFrom[i].value!=replacementsTo[i].value) {
                            user.character.replacements.push([replacementsFrom[i].value, replacementsTo[i].value])
                        }
                    }
                    user.character.regexes = [];
                    var regexesFrom = $('#settings').find('input[name="regex_from"]');
                    var regexesTo = $('#settings').find('input[name="regex_to"]');
                    for (i=0; i<regexesFrom.length; i++) {
                        if (regexesFrom[i].value!="" && regexesFrom[i].value!=regexesTo[i].value) {
                            user.character.regexes.push([regexesFrom[i].value, regexesTo[i].value])
                        }
                    }
                    closeSettings();
                });
            }
            return false;
        });
        
        $('#metaOptions input').click(function () {
            if ($(this).is(':checked')){
                setFlag($(this).prop('id'), 'on');
            } else {
                setFlag($(this).prop('id'), 'off');
            }
        });
        
        /*
        // Clicking the title induces a chat-change function

        $('#chat-title').on('click', function () {
            setSidebar('chatList');
        });
        */

        $('#chatListAdd').on('click', function () {
            $.getJSON('/chats.json', function (data){
                $('#chatPick .list').empty();
                for (i in data.chats) {
                    var chatData = data.chats[i];
                    if (chats.indexOf(chatData.url)<0) {
                        $('<div>').prop('id', chatData.url).addClass('card selection').appendTo('#chatPick .list').on('click', function () {
                            addChat($(this).prop('id'));
                            setSidebar('chatList');
                        });;
                        $('<h1>').addClass('titi').text(chatData.title).appendTo('#'+chatData.url.replace(/\//g, "\\/"));
                    }
                }
            });
            setSidebar('chatPick');
        });

        $('.chatListCancel').on('click', function () {
            setSidebar('userList');
        });

        $('#cancel-characters').on('click', function() {
            setSidebar('settings');
        });

        $('.change-character').on('click', function() {
            $('#change-character-list').html('');
            $.getJSON('/characters.json', function (data) {
                for (i in data.characters) {
                    character = data.characters[i];
                    character_text = '<div style="text-align: center;">'+character.title+'</div><div style="font-size: 14px;">'+
                        '<span style="color:#'+character.color+';">'+character.name+'</span>'
                        +(character.alias?' [<span style="color:#'+character.color+';">'+character.alias+'</span>]':'')+
                    '</div><a href="" class="select-character" style="display: block;text-align: center;">[CHOOSE]</a>';
                    if (i != data.characters.length-1) {
                        character_text += '<hr>';
                    }
                    $('<div>').addClass('characterSwitch').prop('id', 'character-'+character.id).html(character_text).appendTo('#change-character-list');
                }
            });
            setSidebar('character-list');
        });

        $('#change-character-list').on('click', '.select-character', function(e) {
            e.preventDefault();
            switchCharacter($(this).parent().prop('id').substr(10));
            setSidebar('userList');
        });

        $('#hide-topic').click(function () {
            if (user.meta.show_description) {
                $('#hide-topic .text').text('Show Topic');
                $('#topic').hide();
            } else {
                $('#hide-topic .text').text('Hide Topic');
                $('#topic').show();
            }
            alterMeta("show_description", !user.meta.show_description);
        });

        $('#convo').on('click', '.ooc', function () {});

        $(CONVERSATION_CONTAINER).scroll(function (){
            var von = $(CONVERSATION_CONTAINER).scrollTop()+$(CONVERSATION_CONTAINER).height()+24;
            var don = $(CONVERSATION_CONTAINER).prop("scrollHeight");
            var lon = don-von;
            if (lon <= 30){
                $(MISSED_MESSAGE_COUNT_ID).html(0);
            }
        });
        
        $('#textInput').scroll(function () {
            $('#aliasOffset').css('top', (5-$('#textInput').scrollTop())+'px');
        });

        $('#extain').click(function (){
            $(MISSED_MESSAGE_COUNT_ID).html(0);
            $(CONVERSATION_CONTAINER).scrollTop($(CONVERSATION_CONTAINER).prop("scrollHeight"));
        });

        $(window).focus(function (e) {
            if (navigator.userAgent.indexOf('Chrome')!=-1) {
                // You can't change document.title here in Chrome. #googlehatesyou
                window.setTimeout(function () {
                    document.title = CHAT_NAME+' – '+ORIGINAL_TITLE;
                    missed_messages = 0;
                }, 200);
            } else {
                document.title = CHAT_NAME+' – '+ORIGINAL_TITLE;
                missed_messages = 0;
            }
        });
        $(window).unload(function () {
            $.ajax(CHAT_QUIT, {'type': 'POST', data: {'chat_id': chat['id']}, 'async': false});
        });
    }
    
    window.onbeforeunload = function (e) {
        if (typeof e!="undefined") {
            e.preventDefault();
        }
        return 'Are you sure you want to leave? Your chat is still running.';
    }
    
});

