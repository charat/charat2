$(function (){
    $('a.set-as-default').on("click", function() {
        defaultURL = $(this).prop('href');
        $.post(defaultURL, function() {
            location.reload();
        });
        return false;
    });
});