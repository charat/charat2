var search = (function() {

	var characterSelect = $("#characterSelect");
	var characterTags = $("input[name=\"character_wanted\"]");
	var fandomTags = $("input[name=\"fandom_wanted\"]");
	var genderTags = $("input[name=\"gender_wanted\"]");
	var miscTags = $("input[name=\"misc\"]");
	var searchButton = $("#searchButton").click(startSearch);

	var searching = false;
	var searcher_id;

	function startSearch() {
		if (!searching) {
			searching = true;
			$(document.body).addClass("searching");
			$.post("/search", {
				"character_id": characterSelect.val(),
				"character_wanted": characterTags.val(),
				"fandom_wanted": fandomTags.val(),
				"gender_wanted": genderTags.val(),
				"misc": miscTags.val(),
			}, function(data) {
				searcher_id = data.id;
				continueSearch();
			}).error(function() {
				searching = false;
				$(document.body).removeClass("searching");
				alert("Sorry, there seems to be a problem with searching :(");
			});
		}
	}

	function continueSearch() {
		if (searching) {
			$.post("/search/continue", { "id": searcher_id }, function(data) {
				console.log(data);
				if (data.status == "matched") {
					searching = false;
					window.location.href = "/" + data.url;
				} else if (data.status == "quit") {
					searching = false;
				} else {
					continueSearch();
				}
			}).error(function() {
				window.setTimeout(function() {
					searching = false;
					startSearch();
				}, 2000);
			});
		}
	}

	function stopSearch() {
		searching = false;
		$.ajax("/search/stop", { "type": "POST", data: { "id": searcher_id }, "async": false });
		$(document.body).removeClass("searching");
	}

	$(window).unload(function () {
		if (searching) {
			stopSearch();
		}
	});

	return {
		"startSearch": startSearch,
		"stopSearch": stopSearch,
	};

})();

