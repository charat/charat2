$(function(){
	$('.chat-request .toggle-more').on('click', function() {
		if (!$(this).parent().find('.more').is(':visible')) {
			$(this).parent().find('.more').show();
			$(this).find('.text').text('Show Less');
		} else {
			$(this).parent().find('.more').hide();
			$(this).find('.text').text('Show More');
		}
	});

	$('.chat-request .prompt-toggle').on('click', function() {
		if (!$(this).parent().find('.prompt-finish').is(':visible')) {
			$(this).parent().find('.prompt-finish').show();
			$(this).parent().find('.prompt-ellipse').hide();
			$(this).text('[less]');
		} else {
			$(this).parent().find('.prompt-finish').hide();
			$(this).parent().find('.prompt-ellipse').show();
			$(this).text('[more]');
		}
	});
});