import os

from flask import Flask, abort, g, redirect, send_from_directory, url_for, request

from charat2.model.connections import (
    db_commit,
    db_disconnect,
    redis_connect,
    redis_disconnect,
    set_cookie,
)
from charat2.views import root, account, admin, errors, rp, blog, settings
from charat2.views.rp import chat, chat_api, chat_list, characters, request_search, search

from flask.ext.babel import Babel, gettext
from datetime import datetime

from flask.ext.mail import Mail

from charat2.views.root import generate_confirmation_token, confirm_token, send_email
from charat2.model.connections import use_db

app = Flask(__name__)
mail = Mail(app)

app.url_map.strict_slashes = False

app = Flask(__name__)

app.debug = True

app.config.update(dict(
    SERVER_NAME = os.environ["BASE_DOMAIN"],
    RECAPTCHA_SITE_KEY = "6Ley0CATAAAAAPub9QrURSCJHnudPhdlU2lrH8xl",
    RECAPTCHA_SECRET_KEY = "6Ley0CATAAAAAF2vfm01am_GgTdl-5gLL2EU0m5r",
    SECRET_KEY = "salty_lagoon",
    SECURITY_PASSWORD_SALT = "super_salty_lagoon",
    MAIL_SERVER = 'smtp.gmail.com',
    MAIL_PORT = 587,
    MAIL_USE_TLS = True,
    MAIL_USE_SSL = False,
    MAIL_DEFAULT_SENDER = 'help@charatrp.com',
    MAIL_USERNAME = 'help@charatrp.com',
    MAIL_PASSWORD = 'wexKdplLJ^5AI%xc',
))

mail = Mail(app)

babel = Babel(app)
app.jinja_env.globals.update(gettext=gettext)

app.before_request(redis_connect)

app.after_request(set_cookie)
app.after_request(db_commit)

app.teardown_request(db_disconnect)
app.teardown_request(redis_disconnect)

# Time Since Epoch in Seconds
@app.template_filter('TSE')
def datetime_to_epoch(dt):
    epoch = datetime.utcfromtimestamp(0)
    delta = dt - epoch
    return delta.total_seconds()

def make_rules(subdomain, path, func, formats=False, paging=False):
    name = (subdomain + "_" + func.__name__) if subdomain else func.__name__
    if subdomain=="rp":
        subdomain = ""
    else:
        subdomain = subdomain
    app.add_url_rule(path, name, func, subdomain=subdomain, methods=("GET",))
    if formats:
        app.add_url_rule(path + ".<fmt>", name, func, subdomain=subdomain, methods=("GET",))
    if paging:
        app.add_url_rule(path + "/<int:page>", name, func, subdomain=subdomain, methods=("GET",))
    if formats and paging:
        app.add_url_rule(path + "/<int:page>.<fmt>", name, func, subdomain=subdomain, methods=("GET",))

# 1. Root domain (charat.net)

app.add_url_rule("/", "home", rp.home, methods=("GET",))

app.add_url_rule("/login", "log_in", account.log_in_get, methods=("GET",))
app.add_url_rule("/login", "log_in_post", account.log_in_post, methods=("POST",))
app.add_url_rule("/login.<fmt>", "log_in_post", account.log_in_post, methods=("POST",))
app.add_url_rule("/logout", "log_out", account.log_out, methods=("POST",))
app.add_url_rule("/register", "register", account.register_get, methods=("GET",))
app.add_url_rule("/register", "register_post", account.register_post, methods=("POST",))
app.add_url_rule("/confirm/<token>", "confirm_email", account.confirm_email)

app.add_url_rule("/settings", "settings", settings.settings_get, methods=("GET",))
app.add_url_rule("/settings", "settings_post", settings.settings_post, methods=("POST",))

# 2. RP subdomain (rp.charat.net)

app.add_url_rule("/", "rp_home", rp.home, methods=("GET",))

@app.route("/favicon.ico")
def rp_favicon():
    return send_from_directory(os.path.join(app.root_path, "static"), "img/favicons/rp/favicon.ico", mimetype="image/vnd.microsoft.icon")

@app.route("/static/js/rp/log.js")
def rp_log_js():
    return send_from_directory(os.path.join(app.root_path, "static"), "js/rp/log.js")
    
@app.route("/robots.txt")
def robots_txt():
    return send_from_directory(os.path.join(app.root_path, "static"), "robots.txt")

@app.route("/chat/<chat>")
def redirect_chat(chat):
    return redirect("http://rphistory.org/charat/"+chat);
    
@app.route("/chat/<chat>/log", methods=('GET',))
def redirect_log(chat):
    return redirect("http://rphistory.org/charat/"+chat+"/"+request.args.get('page', ''));

# 2.1. Chats list

make_rules("rp", "/chats", chat_list.chat_list, formats=True, paging=True)
make_rules("rp", "/chats/<type>", chat_list.chat_list, formats=True, paging=True)

# 2.2. Character management

make_rules("rp", "/characters", characters.character_list, formats=True)

app.add_url_rule("/characters/new", "rp_new_character_get", characters.new_character_get, methods=("GET",))
app.add_url_rule("/characters/new", "rp_new_character_post", characters.new_character_post, methods=("POST",))

make_rules("rp", "/characters/<int:character_id>", characters.character, formats=True)

app.add_url_rule("/characters/<int:character_id>/save", "rp_save_character", characters.save_character, methods=("POST",))
app.add_url_rule("/characters/<int:character_id>/delete", "rp_delete_character_get", characters.delete_character_get, methods=("GET",))
app.add_url_rule("/characters/<int:character_id>/delete", "rp_delete_character_post", characters.delete_character_post, methods=("POST",))
app.add_url_rule("/characters/<int:character_id>/set_default", "rp_set_default_character", characters.set_default_character, methods=("POST",))

# 2.3. Creating chats

app.add_url_rule("/create_chat", "rp_create_chat", chat.create_chat, methods=("POST",))

# 2.4. Searching

app.add_url_rule("/search", "rp_search", search.search_get, methods=("GET",))
app.add_url_rule("/search", "rp_search_post", search.search_post, methods=("POST",))
app.add_url_rule("/search/continue", "rp_search_continue", search.search_continue, methods=("POST",))
app.add_url_rule("/search/stop", "rp_search_stop", search.search_stop, methods=("POST",))
app.add_url_rule("/characters/<int:character_id>/search", "rp_search_character", search.search_get, methods=("GET",))

# 2.5. Request searching

make_rules("rp", "/requests", request_search.request_list, formats=True, paging=True)
make_rules("rp", "/requests/yours", request_search.your_request_list, formats=True, paging=True)
make_rules("rp", "/requests/<tag_type>:<name>", request_search.tagged_request_list, formats=True, paging=True)

app.add_url_rule("/requests/new", "rp_new_request_get", request_search.new_request_get, methods=("GET",))
app.add_url_rule("/requests/new", "rp_new_request_post", request_search.new_request_post, methods=("POST",))

app.add_url_rule("/requests/answer/<int:request_id>", "rp_answer_request", request_search.answer_request, methods=("POST",))

app.add_url_rule("/requests/edit/<int:request_id>", "rp_edit_request_get", request_search.edit_request_get, methods=("GET",))
app.add_url_rule("/requests/edit/<int:request_id>", "rp_edit_request_post", request_search.edit_request_post, methods=("POST",))

app.add_url_rule("/requests/delete/<int:request_id>", "rp_delete_request_get", request_search.delete_request_get, methods=("GET",))
app.add_url_rule("/requests/delete/<int:request_id>", "rp_delete_request_post", request_search.delete_request_post, methods=("POST",))

# 2.5. Rooms

make_rules("rp", "/rooms", rp.rooms, formats=True)

# 2.6. Chats

make_rules("rp", "/<path:url>", chat.chat, formats=True)

# Push the previous rules to the bottom so it doesn't catch /chats.json, /rooms.json etc.
app.url_map._rules[-2].match_compare_key = lambda: (True, 2, [])
app.url_map._rules[-1].match_compare_key = lambda: (True, 1, [])

make_rules("rp", "/<path:url>/log", chat.log, formats=True, paging=True)

make_rules("rp", "/<path:url>/users", chat.users, formats=True, paging=True)
make_rules("rp", "/<path:url>/invites", chat.invites, formats=True, paging=True)
app.add_url_rule("/<path:url>/uninvite", "rp_chat_uninvite", chat.uninvite, methods=("POST",))
app.add_url_rule("/<path:url>/invite", "rp_chat_invite", chat.invite, methods=("POST",))
app.add_url_rule("/<path:url>/unban", "rp_chat_unban", chat.unban, methods=("POST",))
app.add_url_rule("/<path:url>/subscribe", "rp_chat_subscribe", chat.subscribe, methods=("POST",))
app.add_url_rule("/<path:url>/unsubscribe", "rp_chat_unsubscribe", chat.unsubscribe, methods=("POST",))

# 2.7. Chat API

app.add_url_rule("/chat_api/messages", "messages", chat_api.messages, methods=("POST",))
app.add_url_rule("/chat_api/send", "send", chat_api.send, methods=("post",))
app.add_url_rule("/chat_api/set_state", "set_state", chat_api.set_state, methods=("POST",))
app.add_url_rule("/chat_api/set_group", "set_group", chat_api.set_group, methods=("POST",))
app.add_url_rule("/chat_api/user_action", "user_action", chat_api.user_action, methods=("POST",))
app.add_url_rule("/chat_api/set_flag", "set_flag", chat_api.set_flag, methods=("POST",))
app.add_url_rule("/chat_api/set_topic", "set_topic", chat_api.set_topic, methods=("POST",))
app.add_url_rule("/chat_api/save", "save", chat_api.save, methods=("POST",))
app.add_url_rule("/chat_api/save_from_character", "save_from_character", chat_api.save_from_character, methods=("POST",))
app.add_url_rule("/chat_api/save_variables", "save_variables", chat_api.save_variables, methods=("POST",))
app.add_url_rule("/chat_api/ping", "ping", chat_api.ping, methods=("POST",))
app.add_url_rule("/chat_api/quit", "quit", chat_api.quit, methods=("POST",))

# 4. Admin

app.add_url_rule("/", "admin_home", admin.home, subdomain="admin", methods=("GET",))

app.add_url_rule("/announcements", "admin_announcements", admin.announcements_get, subdomain="admin", methods=("GET",))
app.add_url_rule("/announcements", "admin_announcements_post", admin.announcements_post, subdomain="admin", methods=("POST",))

app.add_url_rule("/broadcast", "admin_broadcast", admin.broadcast_get, subdomain="admin", methods=("GET",))
app.add_url_rule("/broadcast", "admin_broadcast_post", admin.broadcast_post, subdomain="admin", methods=("POST",))

make_rules("admin", "/users", admin.user_list, formats=True, paging=True)
make_rules("admin", "/users/<username>", admin.user, formats=True)
app.add_url_rule("/users/<username>/set_group", "admin_user_set_group", admin.user_set_group, subdomain="admin", methods=("POST",))

make_rules("admin", "/rooms", admin.rooms, formats=True, paging=True)

make_rules("admin", "/log", admin.log, formats=True, paging=True)

# 5. Error handlers

app.error_handler_spec[None][403] = errors.error_403
app.error_handler_spec[None][404] = errors.error_404
app.error_handler_spec[None][500] = errors.error_500

# XXX dear fucking lord we need traversal

