Chat API
========

All chat API requests must be ``POST`` requests.

They must have a ``chat_id`` parameter, and they must contain a session cookie
belonging to a logged in user who has already visited the main page of the
corresponding chat. If these requirements aren't satisfied, a ``400`` error code
will be returned.

If the user doesn't have access permissions for the chat (for example if they've
been banned or it's a private chat), the server will respond with a ``403``
error code.

.. http:post:: /chat_api/messages

   Fetches new messages from the server. This is a long poll request which
   usually doesn't return a response until someone sends a message, so it may
   take several minutes to complete.

   :param int after: Optional. The ID of the latest message you've received. If
                     there are already messages newer than this one then they
                     will be returned straight away (up to the last 50
                     messages), otherwise this request will wait until a new
                     message is posted before returning a response.

   :param string joining: Optional. Indicates that you're joining the chat. This
                          makes the server respond straight away with the chat
                          and user information, rather than waiting for a new
                          message.

   :>json array messages: A list of new messages. This key is always present,
                          unless the ``exit`` key is present.

   :>json object chat: The chat's metadata. This key is only present when the
                       metadata changes.

   :>json array users: A list of online users. This key is only present when the
                       user list changes.

   :>json string exit: Indicates that the user must leave the chat. When the
                       ``exit`` key is present, it will be the only key in the
                       response. Possible values are:

                       * ``kick`` – The client must wait 10 seconds before
                         making any further requests to this chat.
                       * ``ban`` – The client must not send any further requests
                         to this chat and instead send them to the oubliette.

.. http:post:: /chat_api/send

.. http:post:: /chat_api/set_state

.. http:post:: /chat_api/set_group

.. http:post:: /chat_api/user_action

.. http:post:: /chat_api/set_flag

.. http:post:: /chat_api/set_topic

.. http:post:: /chat_api/save

.. http:post:: /chat_api/save_from_character

.. http:post:: /chat_api/save_variables

.. http:post:: /chat_api/ping

.. http:post:: /chat_api/quit

