.. Charat documentation master file, created by
   sphinx-quickstart on Thu Jun 25 20:21:56 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Charat's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 2

   chat-api


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

