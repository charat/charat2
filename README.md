# charat2

The code behind [charatrp.com](http://charatrp.com/), the roleplaying website for all fandoms, take 2.

To install, run `python setup.py develop` to install the dependencies, then `charat2_init_db` to create the database.
